# Class Assignment 4 Report

## 1. Containers with Docker

The goal of this fourth class assignment, is to use **Docker** to setup a containerized environment to execute the
gradle version of spring basic tutorial application. Therefore, two containers will be deployed, a container that is
going to run the web part of the application, and a container to execute the H2 server database.

**Docker** is an open platform for developing, shipping, and running applications, making it possible to separate the
applications from the infrastructure, which leads to a quicker delivery of production software.

**Docker Image** provides the instructions for creating a Docker container. The *Docker Image* can exist without a
container, but teh opposite cannot happen.

**Docker Container** is defined by its image, and the configuration options. A Docker container image is a lightweight,
standalone, executable package of software that includes everything needed to run an application: code, runtime, system
tools, system libraries and settings.

**Containers** are portable across clouds and OS, being well isolated from other containers and its host machine. They
are very similar to Virtual Machines, but are considered to be lightweight than VMs.

<https://docs.docker.com/get-started/overview/>

### 1.1. Installation and Setup

To install **Docker** in the computer, use the following [link](https://www.docker.com/get-started) to download the
latest version.

For running Docker in MacOS and Windows machines, install *Docker Desktop*. This requires Hyper-v on Windows.

### 1.2. Implementation

After installing **Docker**, run:

```shell
% docker info
```

This previous command will display information regarding the **Docker** installation. The shown information includes the
kernel version, the number of containers and images. The number of images shown is the number of unique images.

![Docker Info](images/dockerInfo.png)

#### 1.2.1. Analysis of the Dockerfile

- Download the [repository](https://bitbucket.org/atb/docker-compose-spring-tut-demo/) which contains an example of a
  docker compose that setups two containers:
    - A container that will run Tomcat and the spring tutorial application;
    - A container that will run H2.

The *Dockerfile* is a text file that consists of instructions to build Docker images, it gathers multiple commands into
a single document to fulfill a single task. Each container has a `Dockerfile`.

If we run the command `docker-compose up` , we will have two containers:

- web container:
  Runs the Tomcat and the spring application.
- db container:
  Executes the H2 database as a server process.

The `docker-compose.yml`, defines the multi-container Docker applications. We can create and start all the services from
this configuration. The database's container has to be initialized first than the web container.

#### 1.2.2. Configuration

After the download of the repository and subsequent analysis of the `Dockerfile` of each container
and `docker-compose.yml` file, the next step is to copy the `Dockerfile` of each container and `docker-compose.yml` to
our repository.

The `Dockerfile` of the database should be copied to the folder `ca4/db` and the `Dockerfile` of the web application
should be copied to the folder `ca4/web`. The `docker-compose.yml` file should be located at `ca4`.

After executing the previous step, update the configuration of the `Dockerfile` to use the gradle version of the spring
application (tut-basic).

The changes should be done, in the following section of the web application (web) `Dockerfile`:

```Go
RUN mkdir -p /tmp/build

WORKDIR /tmp/build/

RUN git clone https://MariaLuisFok@bitbucket.org/MariaLuisFok/devops-20-21-1201772.git

WORKDIR /tmp/build/devops-20-21-1201772/ca4/react-and-spring-data-rest-basic

RUN ./gradlew clean build
```

The application by default is generating a `JAR` file. As the application is going to use
the [Tomcat](http://tomcat.apache.org) server, a `WAR` file has to be generated, the `WAR` file is specific for java web
applications. This file was already generated in the previous assignment (ca3), so we are going to use it.

The file, it is also updated with the name of the `WAR` file in the `Dockerfile`:

```GO
RUN cp ./build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/
```

The **web** `Dockerfile` should look like this:

```Go
FROM tomcat

RUN apt-get update -y

RUN apt-get install -f

RUN apt-get install git -y

RUN apt-get install nodejs -y

RUN apt-get install npm -y

RUN mkdir -p /tmp/build

WORKDIR /tmp/build/

RUN git clone https://MariaLuisFok@bitbucket.org/MariaLuisFok/devops-20-21-1201772.git

WORKDIR /tmp/build/devops-20-21-1201772/ca4/react-and-spring-data-rest-basic

RUN ./gradlew clean build

RUN cp ./build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

EXPOSE 8080
```

The **database** `Dockerfile` doesn't need to be updated and therefore should look like this:

```Go
FROM ubuntu

RUN apt-get update && \
  apt-get install -y openjdk-8-jdk-headless && \
  apt-get install unzip -y && \
  apt-get install wget -y

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app/

RUN wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar

EXPOSE 8082
EXPOSE 9092

CMD java -cp ./h2-1.4.200.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists
```

The `docker-compose.yml` should look like this:

```yml
version: '3'
services:
  web:
    build: web
    ports:
      - "8080:8080"
    networks:
      default:
        ipv4_address: 192.168.33.10
    depends_on:
      - "db"
  db:
    build: db
    ports:
      - "8082:8082"
      - "9092:9092"
    volumes:
      - ./data:/usr/src/data
    networks:
      default:
        ipv4_address: 192.168.33.11
networks:
  default:
    ipam:
      driver: default
      config:
        - subnet: 192.168.33.0/24

```

### 1.4. Build and Run

#### 1.4.1. Spring boot tutorial basic project

- To see all images, their repository and tags, and their size, type in the terminal:

```shell
% docker images
```

![Docker images](images/dockerImages.png)

To build the images follow the next step:

- In the folder of the `docker-compose.yml` execute:

```shell
% docker-compose build
```

The first build was not successful due to a permission error:

![Build Error](images/buildError_bitbucket.png)

Therefore, change the repository to public in bitbucket. After changing the repository to public the build was
successful.

![Build Success](images/build_success.png)

After the build the containers will be created locally and can be seen in the **Docker** app.

![Docker app](images/dockerapp_imagescreated.png)

- To run this "composition", in the folder that contains the yml file, execute:

```shell
% docker-compose up
```

![Compose Up](images/docker_compose_up.png)

![Running](images/docker_runningweb.png)

While running, to see the web page, go to:

<http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/>

![Web Page](images/webpage.png)

The H2 database is available at:

<http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console/>

![H2 Link](images/h2_link.png)

- Change the JDBC URL according to the previous image and Connect.

![H2 Database](images/h2_database.png)

While the container is running we can *enter*  the container.

- To *enter* the web container open a new terminal window (in the folder that contains the yml file) and type:

```shell
% docker-compose exec web bash
```

![Web bash](images/enterweb.png)

- To exit the container type:

```shell
% exit
```

- At the end, stop the containers using the Container ID:

```shell
% docker stop 19d5dfb01adb
```  

```shell
% docker stop 44cf36e42cd4
```

![DockerStop](images/docker_stop.png)

![StopContainer](images/stopContainer.png)

### 1.5. Publish the images (db and web) to Docker Hub

To publish the images to [Docker Hub](https://hub.docker.com), the local images must be named using the Docker Hub
username, and the repository name that was created through Docker Hub on the web.

The repository created int Docker Hub is named `devops-20-21-1201772`.

We can add multiple images to a repository by adding a specific `tag` to them. If it’s not specified, the tag defaults
to latest.

To name our we can use several methods:

- When building them, using docker build -t <hub-user>/<repo-name>[:tag];
- By re-tagging an existing local image;
- By using docker commit.

The used method was the *re-tagging*. To see the container tags type:

```shell
% docker images
```

![Docker images](images/dockerImages_db_web.png)

- To tag the web container, type in the terminal:

```shell
% docker tag ca4_web marialuis/devops-20-21-1201772:web
```

- Push the web container/image to the repository:

```shell
% docker push marialuis/devops-20-21-1201772:web
```

To see the updated container tag, type:

```shell
% docker images
```

![Push Web](images/dockerPushWeb.png)

- Re-tag the db container:

```shell
% docker tag ca4_db marialuis/devops-20-21-1201772:db
```  

- Push the database container/image to the repository:

```shell
% docker push marialuis/devops-20-21-1201772:db
```

![Push db](images/dockerPush_db.png)

The containers can be seen in the correspondent repository in [Docker Hub](https://hub.docker.com).

![Docker Hub](images/dockerHub.png)

### 1.6. Copy of the database file to the volume

Volumes are used by **Docker** containers for persisting data, enabling the connection of specific filesystem paths of
the container back to the host machine.

There are two main types of volumes. The *named volumes*, and the *bind mounts*. These are the main two supported by a
default **Docker** engine installation. Nevertheless, there are many volume driver plugins available to support other
tools while using **Docker**.

*Named volumes* will store data, but won't let us specify where the data is to be stored. With *bind mounts*, we control
the exact location on the host. *Bind mounts* are also used to persist data and to provide additional data into
containers.

To copy the database file by using the *exec* to run a shell in the container and coping the database file to a volume
with the db container. The first step is to guarantee that the containers are running. After checking, follow the next
steps.

- Open a new terminal and type:

```shell
% docker exec ca4_db_1 cp jpadb.mv.db ../data
```

![Exec Copy](images/execCopy.png)

- Check if the database file is in the folder (`ca4/data`):

![Exec Copy](images/databasefile.png)

## 2. Alternatives

### 2.1. Kubernetes

**Kubernetes** is an open-source system for automating deployment, scaling, and management of containerized
applications.

To learn more about **Kubernetes** you can follow
this [tutorial](https://kubernetes.io/docs/tutorials/kubernetes-basics/).

**Kubernetes** does not include functionality for creating or managing container images, and it does not, by itself, run
containers; it needs to work with an external container source and runtime.

**Kubernetes** provides a framework for defining applications and orchestrating containers, at scale, and it has a large
community of users and developers, with a corresponding large number of add-ons and support tools.

The main *keywords* of **Kubernetes** are:

**The Control Plane** - The Master.
**Nodes** - Where pods get scheduled.
**Pods** - Holds containers.

### 2.1.1. Kubernetes and Docker

**Docker** is a containerization platform, and Kubernetes is a container orchestrator for container platforms like **
Docker**.
**Kubernetes** it is a comprehensive system for automating deployment, scheduling and scaling of containerized
applications, and supports many containerization tools such as Docker.
**Kubernetes** is a container orchestration system for Docker containers that is more extensive than **Docker**  and is
meant to coordinate clusters of nodes at scale in production efficiently. It works around the concept of pods, which are
scheduling units (and can contain one or more containers) in the Kubernetes ecosystem, and they are distributed among
nodes to provide high availability.

**Kubernetes** and **Docker** are both fundamentally different technologies, but they complement each other, the major
difference between **Docker** and **Kubernetes** is that **Docker** runs on a single node, whereas **Kubernetes** runs
across a cluster. In addition, **Docker** can be used without **Kubernetes**, while **Kubernetes** needs a container in
order to orchestrate.

### 2.1.1. Kubernetes - How it works

When you deploy Kubernetes, you get a cluster.

As previously referred, **Kubernetes** will deploy a cluster, this consists of a set of worker machines, called nodes,
that run containerized applications. Every cluster has at least one worker node.

The worker node(s) host the Pods that are the components of the application workload. The control plane manages the
worker nodes and the Pods in the cluster. In production environments, the control plane usually runs across multiple
computers, and a cluster usually runs multiple nodes.

### 2.2. Heroku

**Heroku** is a container-based cloud Platform used to deploy, manage, and scale apps. The main advantage of **Heroku**
is that it is fully managed and abstracts the developers from maintaining the servers, hardware, or infrastructure and
allowing them to fully focus on the development of their product.

#### 2.2.1. Heroku and Docker

**Heroku** provides two ways of deploying with **Docker**:

- [Container Registry](https://devcenter.heroku.com/articles/container-registry-and-runtime) this option will deploy
  pre-built Docker images to Heroku;
- [Build Docker images](https://devcenter.heroku.com/articles/build-docker-images-heroku-yml) this option provides the
  possibility to build the **Docker** images with `heroku.yml` for deployment to Heroku.

##### 2.2.1.1. Installation and Setup

To use **Heroku** sign up for a free account. If you wish to do an initial tutorial with a Gradle app
click [here](https://devcenter.heroku.com/articles/getting-started-with-gradle-on-heroku).

To install **Heroku** in the computer, use the
following [link](https://devcenter.heroku.com/articles/getting-started-with-gradle-on-heroku#set-up) to download the
latest version.

The installation is also available via Homebrew on macOS.

```shell
% brew install heroku/brew/heroku
```

After the installation and to do the authentication run the `heroku login` command in the terminal:

```shell
% heroku login
```

![Heroku Login](images/herokuLogin.png)

![Heroku Login Wb](images/herokuLoggedIn.png)

##### 2.2.2.2. Implementation

The option to deploy with **Docker** using `heroku.yml` has support for the multi-container. In this option
a `heroku.yml` file needs to be created.

- Create a new application on **Heroku**, while at the root of the application (in this case located
  at `ca4/docker_heroku_tutbasic/`):

```shell
% heroku create tut-basic-app
```  

- Create a `heroku.yml` file in the application's root directory (in this case located at `ca4/docker_heroku_tutbasic/`)
  ;

- Add and commit the `heroku.yml` file to the **Heroku** repository:

```shell
% git add heroku.yml
% git git commit -m "add heroku.yml" 
``` 

- Set the stack of your app to container:

```shell
% heroku stack:set container
```

- Push the app to the Heroku repository:

```shell
% git push heroku master
```

Using this option it was not possible to deploy the app to **Heroku**. The other option is using the *Container
Registry*. To apply the *Container Registry*, follow the next steps:

- Login into **Heroku**, type in the terminal:

```shell
% heroku login
```

```shell
% heroku container:login
```

- Create the app in **Heroku**:

```shell
% heroku create tut-basic-app
```

![Heroku Create](images/herokuCreate.png)

- Create the web container:

```shell
% docker build -t registry.heroku.com/tut-basic-app/web .
```

![Heroku Docker Build](images/dockerBuildRegistry.png)

- Push the image to the registry:

```shell
% docker push registry.heroku.com/tut-basic-app/web
```

![Heroku Docker Build](images/dockerRegistryPush.png)

- Release the image :

```shell
heroku container:release --app tut-basic-app/web
```

![Heroku Docker Build](images/herokuContainerRelease.png)

- Open the app in **Heroku**:

```shell
heroku open --app tut-basic-app/web
```

After deploying the container web to heroku, the web page did not open.

## 3. Mark the repository with the tag ca4

Commit the changes made in the readme

```shell
% git commit -a -m "update readme file"
% git push origin master
```

To mark the end of this assignment with the tag ca4 in the repository:

```shell
% git tag -a ca4 -m "end of class assignment 4p"
% git push origin ca4
```

## 4. References

<https://www.docker.com/resources/what-container>
<https://docs.docker.com/engine/reference/builder/>
<https://docs.docker.com/engine/reference/commandline/info/>
<https://docs.docker.com/docker-hub/repos/#pushing-a-docker-container-image-to-docker-hub>
<https://docs.docker.com/get-started/05_persisting_data/#container-volumes>
<https://docs.docker.com/storage/volumes/>
<https://www.sumologic.com/blog/kubernetes-vs-docker/>
<https://azure.microsoft.com/pt-pt/topic/kubernetes-vs-docker/>
<https://www.ibm.com/cloud/blog/kubernetes-vs-docker>
<https://devcenter.heroku.com/articles/build-docker-images-heroku-yml>
<https://devcenter.heroku.com/articles/git>
<https://semaphoreci.com/blog/kubernetes-vs-docker>
<https://kubernetes.io>
<https://kubernetes.io/docs/tutorials/kubernetes-basics/>
<https://www.freecodecamp.org/news/kubernetes-vs-docker-whats-the-difference-explained-with-examples/>