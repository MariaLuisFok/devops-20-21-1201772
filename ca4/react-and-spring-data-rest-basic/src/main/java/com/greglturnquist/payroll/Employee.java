/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.greglturnquist.payroll;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * @author Greg Turnquist
 */
// tag::code[]
@Entity // <1>
public class Employee {

    private @Id
    @GeneratedValue
    Long id; // <2>
    private String firstName;
    private String lastName;
    private String description;
    private String job;
    private String email;

    private Employee() {
    }

    public Employee(String firstName, String lastName, String description, String job, String email) {

        this.firstName = firstName;
        this.lastName = lastName;
        this.description = description;
        this.job = job;
        this.email = email;
        validate();
    }

    private void validate() {
        validateEmail();
        validateFirstName();
        validateLastName();
        validateDescription();
        validateJob();

    }

    /**
     * Method to validate if an email is valid, i.e if not null, empty and if in the correct format.
     */
    private void validateEmail() {
        if (this.email == null || this.email.isEmpty() || this.email.trim().length() == 0) {
            throw new IllegalArgumentException("Email Address cannot be null or empty.");
        }

        if (!validateEmailFormat(this.email)) {
            throw new IllegalArgumentException("E-mail Address does not have a valid format.");
        }
    }

    /**
     * Method to validate if the first name is valid, i.e if not null or empty.
     */
    private void validateFirstName() {
        if (this.firstName == null || this.firstName.isEmpty() || this.firstName.trim().length() == 0) {
            throw new IllegalArgumentException("First Name cannot be null or empty.");
        }

        if (!validateNameFormat(this.firstName)) {
            throw new IllegalArgumentException("First Name does not have a valid format.");
        }
    }

    /**
     * Method to validate if the last name is valid, i.e if not null or empty.
     */
    private void validateLastName() {
        if (this.lastName == null || this.lastName.isEmpty() || this.lastName.trim().length() == 0) {
            throw new IllegalArgumentException("Last Name cannot be null or empty.");
        }

        if (!validateNameFormat(this.lastName)) {
            throw new IllegalArgumentException("Last Name does not have a valid format.");
        }
    }

    /**
     * Method to validate if the description is valid, i.e if not null or empty.
     */
    private void validateDescription() {
        if (this.description == null || this.description.isEmpty() || this.description.trim().length() == 0) {
            throw new IllegalArgumentException("Description cannot be null or empty.");
        }

        if (!validateNameFormat(this.description)) {
            throw new IllegalArgumentException("Description does not have a valid format.");
        }
    }

    /**
     * Method to validate if the job is valid, i.e if not null or empty.
     */
    private void validateJob() {
        if (this.job == null || this.job.isEmpty() || this.job.trim().length() == 0) {
            throw new IllegalArgumentException("Job cannot be null or empty.");
        }

        if (!validateJobFormat(this.job)) {
            throw new IllegalArgumentException("Job does not have a valid format.");
        }
    }

    /**
     * Method to validate if the email format is according with the regex.
     * Extracted from https://www.geeksforgeeks.org/check-email-address-valid-not-java/
     *
     * @param email email to be validated.
     * @return true if the format is valid, false otherwise.
     */
    private boolean validateEmailFormat(String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        return pat.matcher(email).matches();
    }

    /**
     * Method to validate if the name format is according with the regex.
     * Name is valid with letters, spaces and special characters.
     *
     * @param name name to be validated.
     * @return true if the format is valid, false otherwise.
     */
    private boolean validateNameFormat(String name) {
        String alphaRegex = "^[-'a-zA-ZÀ-ÖØ-öø-ÿ\\s]*$";

        Pattern pat = Pattern.compile(alphaRegex);
        return pat.matcher(name).matches();
    }

    /**
     * Method to validate if the name format is according with the regex.
     * Name is valid with letters, spaces and special characters.
     *
     * @param name name to be validated.
     * @return true if the format is valid, false otherwise.
     */
    private boolean validateJobFormat(String name) {
        String alphaRegex = "^[-'a-zA-ZÀ-ÖØ-öø-ÿ0-9\\s]*$";

        Pattern pat = Pattern.compile(alphaRegex);
        return pat.matcher(name).matches();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return Objects.equals(id, employee.id) &&
                Objects.equals(firstName, employee.firstName) &&
                Objects.equals(lastName, employee.lastName) &&
                Objects.equals(description, employee.description) &&
                Objects.equals(job, employee.job) &&
                Objects.equals(email, employee.email);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, firstName, lastName, description, job, email);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", description='" + description + '\'' +
                ", job='" + job + '\'' +
                ", email='" + email + '\'' + '}';
    }
}
// end::code[]
