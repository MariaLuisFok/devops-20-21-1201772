# Class Assignment 3 Report - Part 2

## 1. Virtualization with Vagrant

On this second part of the third assignment, the goal is to use Vagrant to setup a virtual environment to execute and
run the tutorial spring boot application and gradle "basic" version from the previous class assignments.

### 1.1. Install Vagrant and Setup

- To install the latest version of **Vagrant** follow this [link](https://www.vagrantup.com/downloads).

- Download and install the **Vagrant** version according to your Operating System.

- Check if the installation was successful, by typing in the terminal/console:

```
% vagrant -v
```  

![vagrant version](images/vagrantVersion.png)

### 1.2. Implementation

#### 1.2.1. Analysis of the Vagrantfile

- Download the [repository](https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/) that contains the `Vagrantfile`.

If we run the command vagrant up, we will have two VMs (Virtual Machines):

- web VM:
  Executes the web application inside Tomcat8.
- db VM:
  Executes the H2 database as a server process. The web application connects to this VM.

In the `Vagrantfile`, the following provision is common for both VMs, it will install some basic tools in both VMs, such
as jdk, therefore providing java:

```Ruby
config.vm.provision "shell", inline: <<-SHELL
    sudo apt-get update -y
    sudo apt-get install iputils-ping -y
    sudo apt-get install -y avahi-daemon libnss-mdns
    sudo apt-get install -y unzip
    sudo apt-get install openjdk-8-jdk-headless -y
    # ifconfig
  SHELL
```

The specific configuration for the database VM is as follows. The IP address configured for this VM will
be `192.168.33.11`. The database port will be 9092, according to the `Vagrantfile`.

```Ruby
config.vm.define "db" do |db|
    db.vm.box = "envimation/ubuntu-xenial"
    db.vm.hostname = "db"
    db.vm.network "private_network", ip: "192.168.33.11"
```

When executing the `vagrant up` command, the H2 database will be installed just once:

```Ruby
db.vm.provision "shell", inline: <<-SHELL
      wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar
    SHELL
```

On the other hand, the following provision shell will `always` run , in order to execute the H2 server process, using
java. This is achieved by applying the `always` flag.

```Ruby
db.vm.provision "shell", :run => 'always', inline: <<-SHELL
      java -cp ./h2*.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists > ~/out.txt &
    SHELL
```

The specific configuration for the web VM is as follows. The IP address configured for this VM will be `192.168.33.10`.
The tomcat will be accessible through 8080 port, according to the `Vagrantfile`.

 ```Ruby
 config.vm.define "web" do |web|
    web.vm.box = "envimation/ubuntu-xenial"
    web.vm.hostname = "web"
    web.vm.network "private_network", ip: "192.168.33.10"
 ```

The following provision shell will install the necessary tools for the web VM, such as git, node js, npm and tomcat:

```Ruby
 web.vm.provision "shell", inline: <<-SHELL, privileged: false
      sudo apt-get install git -y
      sudo apt-get install nodejs -y
      sudo apt-get install npm -y
      sudo ln -s /usr/bin/nodejs /usr/bin/node
      sudo apt install tomcat8 -y
      sudo apt install tomcat8-admin -y
```

The database's virtual machine has to be initialized first than the web virtual machine.

#### 1.2.2. Configuration

After the download of the repository and subsequent analysis of the `Vagrantfile`, the next step is to copy
the `Vagrantfile` to our repository. The `Vagrantfile` should be copied to the folder `ca3/part2` for e.g.

After executing the previous step, update the configuration of the `Vagrantfile` to use the gradle version of the spring
application (tut-basic).

The changes should be done, in the following section of the `Vagrantfile`:

```Ruby
# Change the following command to clone your own repository!
     git clone https://MariaLuisFok@bitbucket.org/MariaLuisFok/devops-20-21-1201772.git
      cd ca2/part2/react-and-spring-data-rest-basic
      chmod u+x gradlew
      ./gradlew clean build
      # To deploy the war file to tomcat8 do the following command:
      sudo cp ./build/libs/basic-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps
    SHELL
```

The repository to clone should be our own, and we should change to the directory where the tut-basic application using
gradle is located. The tut-basic application is located at `ca2/part2/react-and-spring-data-rest-basic`, in this
specific case. The command `chmod u+x gradlew` gives execute permissions to the `gradlew`file.

The application by default is generating a `JAR` file. As the application is going to use
the [Tomcat](http://tomcat.apache.org) server, a `WAR` file has to be generated, the `WAR` file is specific for java web
applications. **Gradle** is able to generate the `WAR` file automatically, when building the application.

- To achieve this, the following plugin must be added to the `build.gradle` file in the plugin section:

```groovy
 id 'war'
```

The plugin section in the `build.gradle` file should look like this:

```groovy
plugins {
    id 'org.springframework.boot' version '2.4.4'
    id 'io.spring.dependency-management' version '1.0.11.RELEASE'
    id 'java'
    id "org.siouan.frontend" version "1.4.1"
    id 'war'
}
```

Furthermore, in the dependencies section, a dependency to support the war file to deploying tomcat should be added:

```groovy
providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat'
```

The dependencies section in the `build.gradle` should look like this:

```groovy
dependencies {
    implementation 'org.springframework.boot:spring-boot-starter-data-jpa'
    implementation 'org.springframework.boot:spring-boot-starter-data-rest'
    implementation 'org.springframework.boot:spring-boot-starter-thymeleaf'
    runtimeOnly 'com.h2database:h2'
    testImplementation 'org.springframework.boot:spring-boot-starter-test'
    // To support war file for deploying to tomcat
    providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat'
}
```

- Execute the command to build the project, in the `ca2/part2/react-and-spring-data-rest-basic` folder:

```
% ./gradlew build
```

![gradlew build](images/build_gradle.png)

- If the build was successful, check the name of the war file generated. If necessary update the name in
  the `Vagrantfile`.

  To check the name of the war file generated, check the following path:
  `ca2/part2/react-and-spring-data-rest-basic/build/libs`

![WAR file](images/warFileName.png)

In this case, it is necessary to update the name of the `WAR` file in the `Vagrantfile`:

```Ruby
# Change the following command to clone your own repository!
     git clone https://MariaLuisFok@bitbucket.org/MariaLuisFok/devops-20-21-1201772.git
      cd ca2/part2/react-and-spring-data-rest-basic
      chmod u+x gradlew
      ./gradlew clean build
      # To deploy the war file to tomcat8 do the following command:
      sudo cp ./build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps
    SHELL
```

#### 1.2.3. Changes in the spring boot application (tut-basic)

The necessary steps to complete the configuration of the spring tut-basic application, to apply the H2 server in the
database VM are described in this [repository](https://bitbucket.org/atb/tut-basic-gradle).

These changes must be reproduced in our version of the spring application (tut-basic) from the previous class
assignment (class assignment 2 part2).

- Create a new class `ServletInitializer.java` located at `src/main/java/com/greglturnquist/payroll` with the following
  content:

```java
package com.greglturnquist.payroll;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;


public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ReactAndSpringDataRestApplication.class);
    }
}
```

- Add support for the H2 console. In the `application.properties`, located at `src/main/resources/` add the following:

```java
spring.data.rest.base-path=/api
        spring.datasource.url=jdbc:h2:mem:jpadb
        spring.datasource.driverClassName=org.h2.Driver
        spring.datasource.username=sa
        spring.datasource.password=
        spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
        spring.h2.console.enabled=true
        spring.h2.console.path=/h2-console
```

- Add web allow others to h2, in `application.properties`, located at `src/main/resources/`:

```java
spring.h2.console.settings.web-allow-others=true
```

- Update the path in the `app.js`, located `src/main/js/app.js:

```java
    componentDidMount(){ // <2>
        client({method:'GET',path:'/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/api/employees'}).done(response=>{
        this.setState({employees:response.entity._embedded.employees});
        });
        }
```

- Add the application context path in `application.properties`, located at `src/main/resources/`:

```java
server.servlet.context-path=/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT
```

- Fix reference to css in `index.html`, located at `src/main/resources/templates/`:

```html
<!DOCTYPE html>
<html xmlns:th="https://www.thymeleaf.org">
<head lang="en">
    <meta charset="UTF-8"/>
    <title>ReactJS + Spring Data REST</title>
    <link rel="stylesheet" href="main.css"/>
</head>
<body>

<div id="react"></div>

<script src="built/bundle.js"></script>

</body>

</html>
```

- Add settings for remote H2 database in `application.properties`, located at `src/main/resources/`:

```
#spring.datasource.url=jdbc:h2:mem:jpadb
# In the following settings the h2 file is created in /home/vagrant folder
spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
```

- Add settings in `application.properties`, in order to guarantee that the database is not dropped in every execution,
  located at `src/main/resources/`:

 ```
 # So that spring will no drop de database on every execution.
spring.jpa.hibernate.ddl-auto=update
 ```

In the end the settings of the H2 database should be the following in the `application.properties`, located
at `src/main/resources/` :

 ```
 server.servlet.context-path=/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT
spring.data.rest.base-path=/api
#spring.datasource.url=jdbc:h2:mem:jpadb
# In the following settings the h2 file is created in /home/vagrant folder
spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
spring.datasource.driverClassName=org.h2.Driver
spring.datasource.username=sa
spring.datasource.password=
spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
# So that spring will no drop de database on every execution.
spring.jpa.hibernate.ddl-auto=update
spring.h2.console.enabled=true
spring.h2.console.path=/h2-console
spring.h2.console.settings.web-allow-others=true
 ```

- Add the `WAR` file to the repository, commit and push all the changes to the master.

```git
% git add ca2/part2/react-and-spring-data-rest-basic/build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war

% git commit - m "add war file"
% git push origin master
```

#### 1.2.4. Vagrant

After replicating the changes in our own version of the spring tut basic application, it is time to use Vagrant.

Change your current directory in the terminal to where the `Vagrantfile` is located, in this case it is located
at `ca3/part2`.

To deploy the H2 database Virtual Machine, and the web interface Virtual Machine, type in the terminal:

```
% vagrant up
```

If the repository is private, an error will occur when trying to clone the repository while the `Vagrantfile` is trying
to create and configure the virtual machines. The solution found was to create a token, allowing the access to the
repository. The explanation for the creation and configuration of the token can be
found [here](https://confluence.atlassian.com/bitbucketserver/personal-access-tokens-939515499.html#Personalaccesstokens-Creatingpersonalaccesstokens)
.

The `Vagrantfile` was updated to include the token:

```Ruby
git clone https://MariaLuisFok:peNCQmvbNwLjqC86A2fT@bitbucket.org/MariaLuisFok/devops-20-21-1201772.git
      cd devops-20-21-1201772/ca2/part2/react-and-spring-data-rest-basic
      chmod u+x gradlew
      ./gradlew clean build
      # To deploy the war file to tomcat8 do the following command:
      sudo cp ./build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps
    SHELL

```

After these changes, it is necessary to stop and destroy the created virtual machines.

- To stop the Virtual Machines type:

```
% vagrant halt
```

- To destroy the Virtual Machines type:

```
% vagrant destroy –f
```

- Start the db VM and web VM again, by typing in the terminal:

```
% vagrant up
```

As an alternative the following command will replace the previous ones:

```
% vagrant reload --provision
```

![vagrant up](images/buildSuccess.png)

To check if the db VM and web VM are running, type in the terminal

```
% vagrant status
```

![vagrant status](images/vagrant_status.png)

Enter the database virtual machine through ssh:

```
% vagrant ssh db
```

![vagrant db](images/vagrant_ssh_db.png)

- Check all the running processes in the VM and confirm that the H2 database is running:

```
% ps -ax
```

![h2 ps](images/h2_ps.png)

Enter the web virtual machine through ssh:

```
% vagrant ssh web
```

![vagrant web](images/vagrant_ssh_web.png)

- Check all the running processes in the VM and confirm that the Tomcat is running:

```
% ps -ax
```

![h2 web](images/tomcat_ps.png)

After completing the above steps and while the VMs are running, we can access the database and webpage.

In the host, to open the spring web application (tut-basic) use one of the following options:

<http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/>

<http://192.168.33.10:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/>

![web](images/localhostWeb.png)

To open the H2 console use one of the following urls:

<http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console>

<http://192.168.33.10:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console>

![h2](images/h2.png)

Before connecting in the H2 console, change the **JDBC URL** to:
jdbc:h2:tcp://192.168.33.11:9092/./jpadb as seen in the following image:
![h2_JDBC_URL](images/h2_JDBCURL.png)

After connecting, you can now check the saved employees in the database:
![h2_JDBC_URL](images/h2_employees.png)

- If we stop one of the Virtual Machines, for e.g. if you stop the db VM the frontend in the webpage will have no data
  to show, since the application was changed to run with the database VM.

### 2. Alternative

#### 2.1. VirtualBox vs Parallels

The alternative chosen to complete this assignment was [Parallels](https://www.parallels.com/eu/), a brief comparison
between **VirtualBox** and **Parallels** will be made.

**VirtualBox** is an open-source software for virtualization. It acts as a hypervisor, creating a VM (virtual machine)
where the user can run another OS (operating system). The operating system where VirtualBox runs is called the *host*
OS. The operating system running in the VM is called the *guest* OS. VirtualBox supports Windows, Linux, or macOS as its
host OS.

The main characteristics and advantages of *VirtualBox* are:

- Free and open-source software;
- Runs multiple operating systems simultaneously;
- Easier software installations;
- Testing and disaster recovery;
- Infrastructure consolidation;
- Portability, it can run on many host operating systems.

**Parallels** it’s designed exclusively with macOS in mind, and it is a Hypervisor type 2, like VirtualBox. It also
creates a VM (virtual machine)
where the user can run another OS (operating system), such as Windows,Linux,macOS and use them together.

The main characteristics and advantages of Parallels are:

- user friendly;
- designed oriented to macOS;
- user interface attractive and intuitive;
- easy installation;
- compatibility with several Vagrant boxes.

The main disadvantages of Parallels are:

- the cost, for this assignment a free trial version was used;
- error messages are hard to understand and to find a solution;
- while creating the VM, it was slower than VirtualBox;
- it only runs on Mac operating systems;
- it needs more disk space (Gigabytes) than VirtualBox.

### 2.2. Implementation of Parallels

#### 2.2.1. Installation and Setup

- To install **Parallels** follow this [link](https://www.vagrantup.com/downloads)

- Download and install the latest version of **Parallels**

![parallels installation](images/ParallelsInstallation.png)

- Copy the `Vagrantfile` to a new directory to implement the alternative, in this case the `Vagrantfile` is located
  at `ca3/part2/alternative`.

To run **Vagrant** with **Parallels** a plugin must be installed. **Parallels** provider is a plugin for Vagrant. To
install the plugin type in the terminal/console:

```
% vagrant plugin install vagrant-parallels
```

![parallels installation](images/vagrantPluginParallels.png)

The previous command is going to install the latest version of the plugin. Nevertheless, to guarantee it, run:

```
% vagrant plugin update vagrant-parallels
```

#### 2.2.2. Implementation

The next step is to update the `Vagrantfile`, in order to run the Virtual Machines using Parallels.

Since the box used for the part 1 of this class assignment is only supported by the **VirtualBox**, it was necessary to
choose another box compatible with Parallels. The chosen box was `bento/ubuntu-16.04`.

To check the compatibility of the boxes check
this [link](https://app.vagrantup.com/boxes/search?utf8=✓&sort=downloads&provider=&q=bento).

```Ruby
Vagrant.configure("2") do |config|
  config.vm.box = "bento/ubuntu-16.04"
```

In the configurations specific to the database VM, change to the `bento/ubuntu-16.04` box and update the private network
IP address:

```Ruby
# Configurations specific to the database VM
config.vm.define "db" do |db|
    db.vm.box = "bento/ubuntu-16.04"
    db.vm.hostname = "db"
    db.vm.network "private_network", ip: "192.168.34.11"
```

In addition, in the configurations specific to the webserver VM change to the `bento/ubuntu-16.04` and update the
private network IP address:

```Ruby
# Configurations specific to the webserver VM
  config.vm.define "web" do |web|
    web.vm.box = "bento/ubuntu-16.04"
    web.vm.hostname = "web"
    web.vm.network "private_network", ip: "192.168.34.10"
```

Usually **Vagrant** can pick the right provider, nevertheless **Vagrant** can force to use a specific provider. For
e.g., to force Vagrant to use **Parallels*:

```
% vagrant up --provider=parallels
```

Another alternative is to specify it in the beginning of the `Vagrantfile` for e.g.:

```Ruby
Vagrant.configure("2") do |config|

  # Prefer Parallels before VirtualBox
  config.vm.provider "parallels"
  config.vm.provider "virtualbox"
end
```

![vagrant up failed](images/failedVagrantUp.png)

The first build was unsuccessful as seen in the previous image due to an error downloading the wrapper. The solution
found was to add to the `Vagrantfile` a *wget*, to force the download of the *Gradle Wrapper*. By doing this, the build
was successful.

```Ruby
 wget https://services.gradle.org/distributions/gradle-6.8.3-bin.zip
```

![vagrant up success](images/alternativeBuildSuccess.png)

![parallels vm](images/parallelsVMs.png)

After the successful build:

- Check if the database VM and webserver VM are running, by typing in the terminal:

```
% vagrant status
```

![vagrant status parallels](images/vagrant_status_paralles.png)

- Enter the database virtual machine through ssh:

```
% vagrant ssh db
```

- Check all the running processes in the VM and confirm that the H2 database is running:

```
% ps -ax
```

- Enter the webserver virtual machine:

```
% vagrant ssh web
```

- Check all the running processes in the VM and confirm that the Tomcat is running:

```
% ps -ax
```

After completing the above steps and while the VMs are running, access the database and webpage.

In the host, to open the spring web application (tut-basic) use one of the following options:

<http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/>

<http://192.168.34.10:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/>

![web](images/ipweb_Parallels.png)

To open the H2 console use one of the following urls:

<http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console>

<http://192.168.34.10:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console>

![h2](images/h2web_Parallels.png)

### 3. Mark the repository with the tag ca3-part2

To mark the end of this assignment with the tag ca3-part2 in the repository:

```
% git tag -a ca3-part2 -m "end of class assignment 3 part 2"
% git push origin ca3-part2
```

## 4. References

<https://www.virtualbox.org/wiki/VirtualBox>

<https://www.computerhope.com/jargon/v/virtualbox.htm>

<https://www.virtualbox.org/manual/ch01.html#virt-why-useful>

<https://www.vagrantup.com/downloads>

<https://www.vagrantup.com/docs/multi-machine>

<https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/src/master/>

<https://confluence.atlassian.com/bitbucketserver/personal-access-tokens-939515499.html#Personalaccesstokens-Creatingpersonalaccesstokens>

<https://parallels.github.io/vagrant-parallels/docs/configuration.html>

<https://github.com/Parallels/vagrant-parallels>

<https://www.vagrantup.com/docs/providers/basic_usage>

<https://kb.parallels.com/en/122843>