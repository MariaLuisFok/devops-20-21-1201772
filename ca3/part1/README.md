# Class Assignment 3 Report - Part 1

## 1. Virtualization with Vagrant

On this first part of the third class assignment, the goal is to run the projects from the previous assignments using
**VirtualBox** with Ubuntu.

**VirtualBox** is a free hypervisor that runs in many host operating systems, such as Windows, Linux and OSX.

*Oracle VM VirtualBox* is a free cross-platform virtualization application. **VirtualBox** is a hypervisor that will
extend the capabilities of the existing computer, so that it can run multiple Operating Systems, inside multiple virtual
machines, at the same time. It allows the installation and running of as many virtual machines as we would like. It is
only limited to the disk space and memory.

*Virtualization* is a process that relies on software to simulate hardware functionality and create a virtual computer
system, abstracted from the actual host machine.

*Hypervisor* is computer software, firmware or hardware that creates and runs virtual machines. *Hypervisor* has two
types. The type 1, native or bare-metal hypervisors runs directly on the host computer hardware and controls the
hardware resources. The type 2, hosted hypervisors, runs within a conventional operating system environment, while the
guest operating system runs as a process on the host.

**VirtualBox** is a type 2 hypervisor.

### 1.1. Installation and Setup

To install **VirtualBox** in the computer, use the following [link](https://www.virtualbox.org/wiki/Downloads) to
download the latest version.

### 1.2. Create a VM (Virtual Machine)

After installing **VirtualBox**, it is time to create a Virtual Machine and install *Linux(Ubuntu)* on the new VM. In
addition to access the guest Operating System, it is required to configure the network of the Virtual Machine.

To create a VM using **VirtualBox** the next steps should be followed:

- Add a new VM, name the VM and choose the Operating System accordingly:

The VM will use 2048 MB RAM.

![virtualbox](images/virtualbox.png)

![nameOS](images/name&OS.png)

- Choose the memory size:

![memory size](images/memorysize.png)

- Add the hard disk, choose the type of file for the virtual hard disk, dynamically allocated storage and the file
  location and size for the hard disk:

![hard disk](images/hardDisk.png)

![file type](images/fileTypeHardDisk.png)

![Dynamic Allocation](images/dynamicallyAllocated.png)

![Location and Size](images/locationSize.png)

After finishing the above steps the Virtual Machine is created, and the network should be configured.
![Virtual Machine](images/VM.png)

To configure the Network, select Settings:
![settings](images/settingsnetwork.png)

- Set Network Adapter 1 as Nat

![adapter1](images/adapter1.png)

- Set Network Adapter 2 as Host-only Adapter (vboxnet0)

From the main menu, select File -> Host Network Manager. After click the create button. A new Host-only network will be
created and added to the list. The `Host-only Adapter` is used to allow the host machine to access the guest machine.

![HNM](images/HostNetworkManager.png)

![vboxnet](images/vboxnet.png)

After in File -> Host Network, set Network Adapter 2 as Host-only Adapter (vboxnet0)

![adapter2](images/adapter2.png)

The next step is to install **Ubuntu 18.04**

- Download the *Ubuntu 18.04* Iso image [here](https://help.ubuntu.com/community/Installation/MinimalCD).

- Select Settings

![settings](images/settingsnetwork.png)

- Select Storage and follow the instructions in the below image:

![storage](images/storage.png)

![ubuntu](images/isoubuntu.png)

After performing the above steps the Virtual Machine is ready for the Ubuntu installation:

- To run the VM click on the *Start* button:

![start](images/start.png)

- The *Ubuntu* installation will start:

![install](images/install.png)

- At the end of the installation, remove the iso disk in the Storage and then finish the installation:

![end installation](images/endInstallation.png)

To remove the Disk from the Virtual Drive

![remove disk](images/removeiso.png)

### 1.2.1 Installation of the dependencies of the projects

- Update the packages

```
% sudo apt update
```

- Install **Git**

```
% sudo apt install git
```

- Install **Gradle**

```
% sudo apt install gradle
```

The sudo command allows us to run programs with the security privileges of another user (by default, as the superuser).

### 1.3. Clone the individual repository inside the VM

To clone the repository into the Virtual Machine, the first step is to start the VM and login.

The SSH server is enabled in the VM, so we can now use ssh to connect to the VM. In the host, in the terminal, type:

```
% ssh maria@192.168.56.5
```

![ssh connection](images/1.2ssh_connection.png)

Where maria is the username of the VM and 192.168.56.5 is the IP of the VM.

To clone the individual repository inside the VM:

- Confirm your working directory

```
% pwd
```

- Create a new folder in the desired location/directory:

```
% mkdir devopsRepository
```

- Clone the repository in the created folder, the repository contains the projects we will work on:

```
% cd devopsRepository
% git clone https://MariaLuisFok@bitbucket.org/MariaLuisFok/devops-20-21-1201772.git
```

![clone repository](images/1.2.clone_repository.png)

### 1.4. Build and execute

#### 1.4.1. Spring boot tutorial basic project

##### 1.4.1.1. Run with Maven

To build and execute the spring boot tutorial basic project from the class assignment 1:

- First, connect to the VM through ssh, type in the terminal:

```
% ssh maria@192.168.56.5
```

- Change to the directory which contains the spring boot tutorial basic project:

```
% cd devopsRepository/devops-20-21-1201772/ca1/tut-basic
``` 

- Execute the run command, to run the application:

```
% ./mvnw spring-boot:run
```

While running the application, the webpage can be seen in the host machine by entering the corresponding application
link in the web browser following this example:

```
<Host_IP_Address>:<Application_Port>
```

In this specific case, the ip address was:

```
192.168.56.5:8080
```

The above will allow us to see the webpage:

![frontend](images/tut-basic-frontend.png)

The webpage needs to seen in the host machine, since Ubuntu does not have a Graphical Unit Interface. No issues were
found while building and executing the spring boot tutorial basic project from the class assignment 1.

##### 1.4.1.2. Run with Gradle

To build the spring boot tutorial basic project from the class assignment 2 part 2, using *Gradle*, it is necessary to
have *Gradle* installed in the virtual machine. In the previous steps an example of how to install *Gradle* can be
found.

- Change to the desired directory from your home directory:

```
% cd devopsRepository/devops-20-21-1201772/ca2/part2/react-and-spring-data-rest-basic
```

- Build the tutorial basic project. The frontend tasks will be executed:

```
% ./gradlew build
``` 

![Build_Gradle](images/buildGradle_tutbasic.png)

- Execute the application using:

```
% ./gradlew bootRun
``` 

![bootRun](images/run_tutbasic_gradle.png)

- Access the webpage in:
  <http://192.168.56.5:8080>

![frontend](images/frontendRun_tutbasic_gradle.png)

- Check the available *Gradle* tasks:

```
% ./gradlew tasks
```

![tasks tut basic](images/gradleTasks_tutbasic.png)

- Execute the copyJar task, to create a copy of the generated jar to a folder named *dist**, from the previous class
  assignment:

```
% ./gradlew copyJar
```

![copyJar](images/gradleCopyJar.png)

The generated jar is located at `build/libs/`.

- Execute the task deleteWebpack that was added to the `build.gradle`file in the previous class assignment, this task
  will delete all the files generated by webpack:

```
% ./gradlew deleteWebpack
```

![delete webpack](images/gradleDeleteWebpack.png)

#### 1.4.2. Gradle basic demo project

- Connect to the VM through ssh, type in the terminal of the host machine:

```
% ssh maria@192.168.56.5
```

- Change to the desired directory from your home directory:

```
% cd devopsRepository/devops-20-21-1201772/ca2/part1/luisnogueira-gradle_basic_demo
```

- Build the tutorial basic demo project:

```
% ./gradlew build
``` 

The first build was not successful, due to the lack of the permissions of the `gradlew` file. Therefore, it was
necessary to give execute permission to the `gradlew` file.

![permission](images/permissionsBuildGradle_demoProject.png)

- To change the permission in the `gradlew` file type:

```
% chmod u+x gradlew
```

![change permission](images/changingPermissionsExecution_gradlew_basic-demo.png)

After finding some difficulties building the project due to the **Gradle** version, an error occur related to the Zip
task.

![wrapper error](images/wraperError_basicDemo_gradle.png)

Therefore, a new version of the gradle was installed using the *sdk man*. Before installing *sdk man*, install zip and
unzip.

- To install Zip and UnZip, type the following in the terminal:

```
% sudo apt-get install zip
``` 

![zip](images/installingZip_basic-demo_Gradle.png)

```
% sudo apt-get install unzip
```

![unzip](images/unzip.png)

- Install using *sdk man*, following the instructions in the *sdk man* webpage:

```
% curl -s "https://get.sdkman.io" | bash
```

```
% source "/home/maria/.sdkman/bin/sdkman-init.sh"
```

To check the installed version, type in the terminal:

```
% sdk version
```

To see the available list tools for installation, type:

```
% sdk list
```

- After the successful installation of *sdk man*, to install **Gradle** type in the terminal:

```
% sdk install gradle
```

![sdk installation](images/installingGradleUsingSDKMAN.png)

After installing the **Gradle** version 7, the build was successful. The first installation of **Gradle** done in the
previous steps was not compatible with this project.

- Build the application:

```
% ./gradlew build
```

![build successful](images/buildSuccessful_demoProject_gradle.png)

- Check the available tasks:

```
% ./gradlew tasks
```

![tasks demoBasic](images/gradleTasks_demoBasic.png)

Execute the server in the VM using:

```
% ./gradlew runServer
```

![runServer](images/runServerVM.png)

While the server is running, and to run various clients, several terminals must be opened, and the runClient gradle task
must be executed:

```
 % ./gradlew runClient
```

![run client](images/gradleRunClient_BuildFailed.png)

The *runClient* task fails since *Ubuntu* does not have a Graphical Unit Interface.

#### 1.4.2.1. Gradle basic demo project - Execute Server in VM and Run Client in host machine

As noted previously, in order to test the client part of the project, the client-side has to run in the host machine,
since Ubuntu does not have a Graphical Unit Interface.

As the Server is now being hosted in the VM , a new task for the client side of the application has to be created in
order to execute the RunClient task and to connect to the of the server instead of the localhost.

Consequently, this task will be added to the `build.gradle` file in our repository.

- Add "runClientVM" task in the `build.gradle`file:

```Groovy
task runClientHost(type: JavaExec, dependsOn: classes) {
    group = "DevOps"
    description = "Launches a chat client that connects to a server on 192.168.56.5:59001 "

    classpath = sourceSets.main.runtimeClasspath

    main = 'basic_demo.ChatClientApp'

    args '192.168.56.5', '59001'
}
```

- Build the application again:

```
% ./gradlew build
```

- Execute the server in the VM using:

```
% gradle runServer
```

- In the host machine in the directory of the project, run the runClientHost task:

```
% gradle runClientHost
```

![run client host](images/runClientHost.png)

### 1.5. Mark the repository with the tag ca3-part1

Commit the changes made in the readme

```
$ git commit -a -m "update readme file"
$ git push origin master
```

To mark the end of this assignment with the tag ca3-part1 in the repository:

```
$ git tag -a ca3-part1 -m "end of class assignment 3 part 1"
$ git push origin ca3-part1
```

## 2. References

<https://www.virtualbox.org>
<https://www.virtualbox.org/manual/UserManual.html#virt-why-useful>
<https://phoenixnap.com/kb/what-is-hypervisor-type-1-2>
<https://www.vmware.com/topics/glossary/content/hypervisor>


