# Class Assignment 2 Report - Part 1

## 1. Build tools with Gradle

On this first part of the second assignment, the goal is to implement a basic multithreaded chat room server, through
the use of **Gradle**.

The first step in this assignment is to download
the [basic_demo application](https://bitbucket.org/luisnogueira/gradle_basic_demo/).

After placing the application in the desired directory, it's necessary to build a `.jar` file:

```
 % gradle build
```

### 1.2. Add a new task to execute the server

#### Run the Server

After building `.jar` file, and to run the application, the command must be typed in the terminal to run the server:

```
 % java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp <server port>
```

The `<server port>` needs be changed to a valid port, e.g. 59001.

#### Run the Clients

While the server is running, and to run various clients, several terminals must be opened, and the runClient gradle task
must be executed:

```
 % ./gradlew runClient
```

To add a new task, in order to run the server the steps below must be followed:

Open the file `build.gradle` and add the following task:

```groovy
task runServer(type: JavaExec, dependsOn: classes) {

    group = "DevOps"
    description = "Launches a server on localhost:59001 "

    classpath = sourceSets.main.runtimeClasspath

    main = 'basic_demo.ChatServerApp'

    args '59001'
}

```

With this task implemented, the command can be used to start the server:

```
% gradle runServer
```

or using the [Gradle Wrapper](https://docs.gradle.org/current/userguide/gradle_wrapper.html#header)

```
% ./gradlew runServer
```

The [Gradle Wrapper](https://docs.gradle.org/current/userguide/gradle_wrapper.html#header) it is the recommended way to
execute any Gradle build. According
to [The Gradle Wrapper](https://docs.gradle.org/current/userguide/gradle_wrapper.html#header):

"The Wrapper is a script that invokes a declared version of Gradle, downloading it beforehand if necessary. As a result,
developers can get up and running with a Gradle project quickly without having to follow manual installation processes
saving time and money."

### 1.3. Add unit test and update the gradle script, to execute the test

To add a new unit tests dependency in gradle, go to the file `build.gradle` and add the following in dependencies:

```groovy
testImplementation 'junit:junit:4.13.1'
```

The dependencies should look like this:

```groovy
dependencies {
    // Use Apache Log4J for logging
    implementation group: 'org.apache.logging.log4j', name: 'log4j-api', version: '2.11.2'
    implementation group: 'org.apache.logging.log4j', name: 'log4j-core', version: '2.11.2'
    testImplementation 'junit:junit:4.13.1'
}
```

In this case it was only necessary to add a new dependency in the file `build.gradle`, since the java plugin is already
in that file.

The code of the unit test can be added to the file `AppTest.java`, located
in [AppTest](../part1/luisnogueira-gradle_basic_demo/src/test/java/basic_demo/AppTest.java):

```java
package basic_demo;

import org.junit.Test;

import static org.junit.Assert.*;

public class AppTest {
    @Test
    public void testAppHasAGreeting() {
        App classUnderTest = new App();
        assertNotNull("app should have a greeting", classUnderTest.getGreeting());
    }
}
```

```
% gradle test
```

or

```
% ./gradlew test
```

For example, if you wish to execute all the tests in just one test class (e.g. `AppTest`) you can use:

```
% gradle test --tests AppTest
```

or if you wish to execute a specific test of a test class (e.g. `AppTest`) you can use:

```
% gradle test --tests AppTest.testAppHasAGreeting
```

### 1.4. Add a new task of type Copy to be used to make a backup of the sources of the application

The purpose of this task is to copy the contents of the _src_ folder to a new _backup_ folder.

According
to [Copying directory hierarchies](https://docs.gradle.org/current/userguide/working_with_files.html#sec:copying_directories_example)
, to create a backup directory with the contents of the src directory we should add the following task to the file
`build.gradle`:

- The task backupCopy will copy the contents of the directory src into the directory backup which will also be created
  during this task:

```groovy
task backupCopy(type: Copy) {

    from layout.buildDirectory.dir('../src')
    into layout.buildDirectory.dir('../backup')
}
```

To create a backup directory with the contents of the src directory run in the terminal the command:

```
% gradle backupCopy
```

or

```
% ./gradlew backupCopy
```

Before merging the changes into master run the task runServer and runClient to confirm if everything is running
correctly:

```
% gradle runServer
% gradle runClient
```

or alternatively, using the Gradle wrapper:

```
% ./gradlew runServer
% ./gradlew runClient
```

### 1.5. Add a new task of type Zip to be used to make an archive (i.e., zip file) of the sources of the application

The purpose of this task is to copy the contents of the src folder to a new zip file.

According
to [Creating archives (zip,tar,etc.)](https://docs.gradle.org/current/userguide/working_with_files.html#sec:creating_archives_example)
, to create a copy of the contents of the src directory to a new zip file we should add the following task to the file
`build.gradle`.

- The task toZip will archive the entire contents of the directory src, by creating a ZIP of the src directory, the
  archive file name will be *src-copy.zip* in the directory named zip:

```groovy
task toZip(type: Zip) {

    archiveFileName = 'src-copy.zip'
    destinationDirectory = layout.buildDirectory.dir('../zip')
    from layout.buildDirectory.dir('../src')
}
```

The task can be run with the command:

```
% gradle toZip
```

or

```
% ./gradlew toZip
```

At the end to guarantee that everything is running correctly, execute the test task :

```
% gradle test
```

### 1.6. Mark the repository with the tag ca2-part1

To mark the end of this assignment with the tag ca2-part1 in the repository:

```
$ git tag -a ca2-part1 -m "end of class assignment 2 part 1"
$ git push origin ca2-part1
```

## 2. References

<https://docs.gradle.org/current/userguide/java_testing.html>

<https://github.com/junit-team/junit4/wiki/Use-with-Gradle>

<https://docs.gradle.org/current/userguide/working_with_files.html#sec:creating_directories_example>

<https://docs.gradle.org/current/userguide/working_with_files.html#sec:creating_archives_example>

<https://docs.gradle.org/current/userguide/gradle_wrapper.html#header>

