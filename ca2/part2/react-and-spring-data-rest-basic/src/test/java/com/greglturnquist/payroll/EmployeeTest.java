package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class EmployeeTest {

    @Test
    void ensureEmployeeCreatedSuccessfully() {
        String firstName = "Samwise";
        String lastName = "Gamgee";
        String description = "Frodo Baggins best friend";
        String job = "Mayor of the Shire";
        String email = "samwise.gamgee@gmail.com";

        Employee anEmployee = new Employee(firstName, lastName, description, job, email);

        assertNotNull(anEmployee);
    }


    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"samwise.gamgee.gmail.com", "gamgee@hotmail", "€samwise@hotmail.com", "samwise.gamgee@gmail.c", "samwise_gamgee@1234.567.89"})
    void ensureEmployeeNotCreatedWithInvalidEmail(String email) {
        String firstName = "Samwise";
        String lastName = "Gamgee";
        String description = "Frodo Baggins best friend";
        String job = "Mayor of the Shire";

        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, job, email));

    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"Samwise1234", "Samwise@", "Samwise_Frodo"})
    void ensureEmployeeNotCreatedWithInvalidFirstName(String name) {
        String firstName = name;
        String lastName = "Gamgee";
        String description = "Frodo Baggins best friend";
        String job = "Mayor of the Shire";
        String emailAddress = "samwise.gamgee@gmail.com";

        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, job, emailAddress));

    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"Gamgee1234", "Gamgee@", "Gamgee_Frodo"})
    void ensureEmployeeNotCreatedWithInvalidLastName(String name) {
        String firstName = "Samwise";
        String lastName = name;
        String description = "Frodo Baggins best friend";
        String job = "Mayor of the Shire";
        String emailAddress = "samwise.gamgee@gmail.com";

        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, job, emailAddress));

    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"elf-friend1234", "elf-firend@", "elf_Friend", "elf-friend*"})
    void ensureEmployeeNotCreatedWithInvalidDescription(String description) {
        String firstName = "Samwise";
        String lastName = "Gamgee";
        String employeeDescription = description;
        String job = "Mayor of the Shire";
        String emailAddress = "samwise.gamgee@gmail.com";

        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, employeeDescription, job, emailAddress));

    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"Wizard*", "Wizard@", "elf_Wizard"})
    void ensureEmployeeNotCreatedWithInvalidJob(String job) {
        String firstName = "Samwise";
        String lastName = "Gamgee";
        String description = "Frodo Baggins best friend";
        String jobTitle = job;
        String emailAddress = "samwise.gamgee@gmail.com";

        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, emailAddress));

    }


}