# Class Assignment 2 Report - Part 2

# 1. Converting the tutorial application from Maven to Gradle

On this second part of the second assignment, the goal is to convert the basic version of the Tutorial application
(tut-basic) to **Gradle**. The basic version of the Tutorial application (tut-basic) is using **Maven**

Gradle is a dependency management, and a build automation tool, which was built upon the concepts of Ant and Maven. The
build automation tool is used to automate the creation of applications.

## 1.1 Create new branch

To complete this part of the assignment a new branch in the repository called tut-basic-gradle must be created.

To create a new branch in the repository and checkout this branch immediately, the command should be typed:

```
% git checkout -b tut-basic-gradle 
```

This command will create and checkout a new branch.

## 1.2 Configuring tutorial application

### 1.2.1 Create a new gradle spring project

As instructed in the readme file of the basic version of the Tutorial application, the **Rest Repositories**,
**Thymeleaf**, **Spring Data JPA** and **H2 Database** should be added to the new gradle spring project. To create the
new gradle spring project with the mentioned dependencies use [spring initializr](https://start.spring.io).

In the [spring initializr](https://start.spring.io), the chosen project must be Gradle Project, Java Language and Jar
Packaging. To add the dependencies, click on the button **
add dependencies**.
![spring intitializr](images/springinitializr.png)

After adding the dependencies, click on the **generate** button to create the new gradle spring project, creating a zip
file.
![spring intitializr](images/springGenerate.png)

### 1.2.2 Extract the generated zip file

After the creation of the gradle spring project, the generated zip file must be extracted. This zip files has to be
extracted inside the folder `ca2/part2/` of the repository. We now have an "empty" spring application that can be built
using gradle.

To check the available gradle tasks execute:

```
% ./gradlew tasks
```

### 1.2.3 Delete src folder and copy src from tutorial application

In this step, the src folder (and all its subfolder) from the extracted zip folder must be deleted and replaced by the
src folder (and all its subfolder) of the basic tutorial application. This step is necessary since the purpose of this
assignment is to work on the code from the basic tutorial application.

To remove the src folder from the extracted zip folder type, it must be inside the folder where the src is located
(`ca2/part2/react-and-spring-data-rest-basic/`):

```
% rm -R src
```

After removing the src folder from the extracted zip, copy the src folder of the basic tutorial application in the same
directory. You can check the folders and files of the current directory by typing:

```
% ls
```

In addition, copy the files `webpack.config.js` and `package.json`in the same directory and delete the folder
`src/main/resources/static/built/`. This folder has to be deleted since it should be generated from the javascript by
the tool webpack.

After completing these steps, the application can be run by using:

```
% ./gradlew bootRun
```

While running, check the web page http://localhost:8080. The http://localhost:8080  is empty since the `build.gradle`
file does not have the plugin necessary to handle the frontend code.

Commit and push the changes made to the tut-basic-gradle branch:

```
% git add .
% git commit -m "close #7,#8,#9,#10,#11"
% git push origin tut-basic-gradle 
```

### 1.2.4 Add the gradle plugin to manage the frontend

As previously stated, in order to see the frontend, it will be necessary to add the gradle
plugin [org.siouan.frontend](https://github.com/Siouan/frontend-gradle-plugin) to the project. By adding this plugin
gradle will be able to manage the frontend.

#### 1.2.4.1 Add plugin in `build.gradle` file

Open the `build.gradle` file and add the following line to the plugins block:

```groovy
id "org.siouan.frontend" version "1.4.1"
```

The plugins section should look like this:

```groovy
plugins {
    id 'org.springframework.boot' version '2.4.4'
    id 'io.spring.dependency-management' version '1.0.11.RELEASE'
    id 'java'
    id "org.siouan.frontend" version "1.4.1"
}
```

Commit the changes made in the `build.gradle` file :

```
% git commit -a -m "close #12, add frontend plugin"
```

#### 1.2.4.2 Configure the previous plugin in the `build.gradle` file:

In addition, to configure the previous plugin it is necessary to add the following code in the `build.gradle` file:

```groovy
frontend {
    nodeVersion = "12.13.1" assembleScript = "run webpack"
}
```

Commit the changes made in the `build.gradle` file :

```
% git commit -a -m "close #14, add code to build.gradle to configure previous plugin"
```

#### 1.2.4.3 Update the scripts section/object in package.json to configure the execution of webpack

To configure the execution of webpack, open the `package.json` and in the *scripts* section add:

```groovy
"webpack" : "webpack"
```

The scripts section should look like this:

```groovy
"scripts" : {
    "watch" : "webpack --watch -d" ,
    "webpack" : "webpack"
} ,
```

Commit the changes made in the `package.json` file :

```
% git commit -a -m "close #14, update the scripts section in package.json"
```

Push all the changes to the tut-basic-gradle branch:

```
% git push origin tut-basic-gradle
```

#### 1.2.4.4 Build and Run the application

After completing all the previous steps build and run the project. The frontend will be generated, since the tasks
related to the frontend will also be executed.

To build the application execute:

```
% ./gradlew build
```

To run the application execute:

```
% ./gradlew bootRun
```

While running, check the web page http://localhost:8080. The http://localhost:8080 now has the frontend.

### 1.2.5 Add a task to gradle to copy the generated jar to a folder named *dist*

The purpose of this issue is to copy the generated jar to a folder named "dist" located at the project root folder
level. The generated jar is located at `build/libs/`.

To create a copy of the generated jar to a folder named "dist" the following task should be added to the `build.gradle`
file:

```groovy
task copyJar(type: Copy) {

    from('build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.jar')
    into('dist')
}
```

To create a *dist* directory with the generated jar execute:

```
% ./gradlew copyJar
```

Commit the changes made in the `build.gradle` file :

```
% git commit -a -m "close #17,#18, add task to copy the jar to a folder named "dist" in build.gradle"
```

Push all the changes to the tut-basic-gradle branch:

```
% git push origin tut-basic-gradle
```

### 1.2.6 Add a task to gradle to delete all the files generated by webpack

The goal of this task is to delete all the files generated by webpack. The files generated by webpack are usually
located at
`src/main/resources/static/built/`. The task should be executed automatically by gradle before the task clean. In order
for this task to execute before the task *clean*, the task *clean* must depend on the deleteWebpack task. Therefore,
since clean depends on deleteWebpack, the deleteWebpack will be executed first.

The following task should be added to the `build.gradle`file:

```groovy

task deleteWebpack(type: Delete) {
    delete fileTree('src/main/resources/static/built/')
}

clean.dependsOn(deleteWebpack)
```

Execute the task deleteWebpack, to delete all the files generated by webpack:

```
% ./gradlew deleteWebpack
```

Commit the changes made in the `build.gradle` file :

```
% git commit -a -m "close #19, add task to gradle to delete all the files generated by webpack"
```

Push all the changes to the tut-basic-gradle branch:

```
% git push origin tut-basic-gradle
```

To confirm that the deleteWebpack task is being executed before the clean task you can execute with --info or --scan

- The --info tag will show the executed tasks in the terminal:

```
% ./gradlew deleteWebpack --info
```

![info](images/gradleCleanInfo.png)

- The --scan tag will publish build scan to scans.gradle.com.

```
% ./gradlew deleteWebpack --scan
```

![scan](images/gradleCleanScan.png)

### 1.2.7. Merge with the master branch

After running and testing all the developed features, commit and push the changes to the master branch.

While on branch tut-basic-gradle check if the master is updated and the status of the files:

```
% git remote update
% git status
% git pull origin master
```

First change to the master branch and then merge the tut-basic-gradle branch into the master, in the end push the
changes to the master.

```
% git checkout master
% git merge tut-basic-gradle
% git push origin master
```

## 2. Alternative Solution

To complete this part of the assignment a new branch in the repository named ca2-alternative-tool must be created.

To create a new branch in the repository and checkout this branch immediately, the command executed was:

```
% git checkout -b ca2-alternative-tool
```

This command will create and checkout a new branch.

## 2.1. Analysis of the alternative

The alternatives chosen to complete this assigment were Ant + Ivy and Ant. In this part of the assignment a brief
comparison between *Gradle*, *Maven* and *Ant* will be made.

The *Apache Ant* (“Another Neat Tool”) is a Java-based tool used for automating building processes for Java
applications. Additionally, Ant can be used for building non-Java applications. *Apache Ivy* was developed as a
sub-project of the Apache Ant project to provide support for dependency management to *Ant*, following the same design
principles.

The main characteristics of *Ant* are:

- The build files (`build.xml`) are written in XML and by convention;
- Provides flexibility (does not impose coding convention or project structures);
- Can lead to extensive XML build files (low maintainability), due to its flexibility;
- Does not provide built-in support for dependency management;
- Steep learning curve.

In the other hand, *Apache Maven* is a dependency management, and a build automation tool, primarily used for Java
applications and can be considered a plugin execution framework.*Maven's* build is in XML and contains build and
dependency management instructions.

The main characteristics of *Maven* are:

- The build file (`pom.xml`) is written in XML;
- Relies on conventions, a strict project structure and provides predefined commands;
- Provides built-in support for dependency management;
- Supports a wide range of available plugins, and each of them can be additionally configured;
- Has the majority of the build tool market as of today.

In addition, *Gradle* is also a dependency management, and a build automation tool that was built upon the concepts of
*Ant* and *Maven*.

The main characteristics of *Gradle* are:

- Smaller configuration file (`build.gradle`);
- Provides little functionality, plugins add all the features;
- Uses a DSL based either on Groovy or Kotlin.

To summarize, *Ant* and *Maven* building files are in XML which can lead to extensive and hard to maintain building
files as opposed to the *Gradle* building file which smaller, easier to understand and maintain. *Maven* and *Gradle*
have built-in support for dependency management, while *Ant* needs *Ivy* to manage its dependencies, in addition *Maven*
and *Gradle* have more available support and resources (documentation) when compared to *Ant*.

## 2.2 Implementation of the alternative

To implement the alternative the first approach was to try to implement the demo basic application using the Apache Ant

+ Ivy. Nevertheless, the implementation using Ant + Ivy was not successful.

### 2.2.1. Installation and setup

- To install the Apache Ant follow the next steps:
    - Install [SDKMAN!](https://sdkman.io/install), this will allow you to have parallel versions of multiple Software
      Development Kits
    - After the installation of the [SDKMAN!](https://sdkman.io/install), type in the terminal:
    ```
    % sdk install ant
    ```
    - To verify the successful installation of Apache Ant on the computer, type in the terminal
   ```
    % ant -version 
    ```

- To install Ivy:
    - Download the last
      version [here](https://ant.apache.org/ivy/download.cgi?Preferred=ftp://ftp.osuosl.org/pub/apache/)
    - Unpack the downloaded zip file and copy the ivy jar file into your ant lib directory (ANT_HOME/lib)
    - To check where the lib directory (ANT_HOME/lib) is located type in the terminal:
    ```
    % ant -diagnostics 
    ```
  See the following example of the report generated by the previous command:
  ![scan](images/antHomeLocation.png)

#### 2.2.2. Implementation with a basic project without spring boot

Following a simple tutorial to implement a basic project using Apache Ant + Ivy. The tutorial can be found
[here](https://www.tutorialspoint.com/apache_ivy/apache_ivy_quick_guide.htm). The `ivy.xml`, `build.xml` and
`Application.java used for this tutorial can also be found in the
previous [link]((https://www.tutorialspoint.com/apache_ivy/apache_ivy_quick_guide.htm)). To build the project type in
the terminal:

```
% ant 
```

![Build](images/antBuild.png)

The `build.xml` file contains the project name, the default task namespace for ivy and the target elements, which will
create a new task and its description.

In this particular case, the `build.xml` has the ivy resolve target. The ivy resolve task will resolve the dependencies
using ivy, when the project is built.

The implementation of this option is documented in the repository in the folder `ivy`.

#### 2.2.3. Implementation with the basic version of the Tutorial application (tut-basic) with Apache Ant + Ivy

In an initial approach, I have tried to implement the basic version of the Tutorial application using Ant + Ivy, since
Ivy would manage the dependencies. After following the instructions provided in the following link to configure the
spring boot dependencies in `build.xml` and `ivy.xml` files in the tut-basic and using the `build.xml` and `ivy.xml`
from the previous example, I have tried to build the project, nevertheless the build failed, due to the fact that the
dependencies of the spring boot framework were not found.

The information and resources available were not sufficient to have the tutorial application (tut-basic) with Ant Ivy
building.

![AntIvyBuild](images/ivyAntBuild.png)

The implementation of this option is documented in the repository in the folder `ant-ivy-tut-basic`.

#### 2.2.4. Implementation with the basic version of the Tutorial application (tut-basic) with Apache Ant

After an unsuccessful attempt to implement the Spring Boot using *Ant + Ivy*, and after reflection and discussion with
the group colleagues, it was decided to implement the alternative using Ant.

To implement Ant, as previously done in [1.2.1](###1.2.1 Create a new gradle spring project), the **Rest Repositories**,
**Thymeleaf**, **Spring Data JPA** and **H2 Database** should be added to the new spring project. In this case, instead
of creating a new project using *Gradle*, choose the *Maven* option instead. To create the new *Maven* spring project
with the mentioned dependencies use [spring initializr](https://start.spring.io).

After downloading the project, extract the generated file into the desired directory.

The next step is to convert the *Maven* project into an *Ant* Project.

- Converting the *Maven* project int an *Ant* project
    - Update the build section in the `pom.xml` file with:

```xml

<build>
    <defaultGoal>install</defaultGoal>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-compiler-plugin</artifactId>
            <configuration>
                <source>8</source>
                <target>8</target>
            </configuration>
        </plugin>
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
        </plugin>
    </plugins>
</build>
```

If the `pom.xml` file is not updated with the previous configurations the following error will occur:

![error](images/errorPom.png)

- To convert the to an *Ant* project execute the command:

```
% ./mvnw ant:ant
```

- To build the project type in the terminal:

```
% ant
```

The `build.xml`file has the tasks (identified as target)  and project settings. When using this conversion the tasks
from *Ant* are imported from the `maven-build.xml`file.

- To check the *Ant* task type:

```
% ant -p
```

The implementation of this option is documented in the repository in the folder `ant-tut-basic`.

#### 2.2.5. Adding tasks

The tasks were added in the three examples mentioned previously.

##### 2.2.5.1. Add a task to copy the generated jar to a folder named *dist*

The purpose of this issue is to copy the generated jar to a folder named "dist" located at the project root folder
level.

To create a copy of the generated jar to a folder named *dist* the following task should be added to the `build.xml`
file:

```xml

<target name="copyJar" description="Copies the generated jar to a folder named dist">
    <copy file="./target/ant-tut-basic-0.0.1-SNAPSHOT.jar" todir="./dist"/>
</target>
```

Execute the task copy, To create a copy of the generated jar to a folder named *dist*:

```
% ant copyJar
```

##### 2.2.5.2 Add a task to delete all the files generated by webpack

The goal of this task is to delete all the files generated by webpack. The files generated by webpack are usually
located at
`src/main/resources/static/built/`. The task *clean* must depend on the deleteWebpack task. Therefore, since clean
depends on deleteWebpack, the deleteWebpack will be executed first.

The following tasks should be added to the `build.xml`file:

```xml

<target name="clean" depends="deleteWebpack" description="cleans the output directory">
    <delete dir="${maven.build.dir}"/>
</target>
```

```xml

<target name="deleteWebpack" description="deletes all the files generated by webpack">
    <delete dir="src/main/resources/static/built/"/>
</target>
```

To execute only the deleteWebpack task, to delete all the files generated by webpack, the command was used:

```
% ant deleteWebpack
```

To execute the task clean and also execute the deleteWebpack task, type in the terminal:

```
% ant clean
```

### 2.2.6. Merge with the master branch

After running and testing all the developed features, commit and push the changes to the master branch.

First change to the master branch and then merge the ca2-alternative-tool branch into the master, in the end push the
changes to the master.

```
% git checkout master
% git merge ca2-alternative-tool
% git push origin master
```

## 3. Mark the repository with the tag ca2-part

To mark the end of this assignment with the tag ca2-part1 in the repository:

```
% git tag -a ca2-part2 -m "end of class assignment 2 part 2"
% git push origin ca2-part2
```

## 4. References

[Spring Initializr] <https://start.spring.io>

[Gradle User Manual] <https://docs.gradle.org/current/userguide/userguide.html>

[Ant vs Maven vs Gradle] <https://www.baeldung.com/ant-maven-gradle>

[Overview of Apache Ant Tasks] <https://ant.apache.org/manual/tasksoverview.html>

[Apache IVY - Quick Guide] <https://www.tutorialspoint.com/apache_ivy/apache_ivy_quick_guide.htm>