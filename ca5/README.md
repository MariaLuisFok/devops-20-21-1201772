# Class Assignment 5 Report - Part 1

## 1. Continuous Integration/ Continuous Development pipelines with Jenkins

The goal of this fifth class assignment, is to use **Jenkins** with the *gradle basic demo* from the class assignment
two, to create a pipeline.

**Jenkins** is an open platform for developing, shipping, and running applications, making it possible to separate the
applications from the infrastructure, which leads to a quicker delivery of production software.

**CI/CD Pipeline** is a set of plugins which support implementing and integrating continuous delivery pipelines into
Jenkins.

A continuous delivery (CD) pipeline is an automated process which is responsible for getting software from version
control right through to the end user. The process consists in building the software and progressing the built
software (*build*) through multiple stages of testing and deployment.

### 1.1. Installation and Setup

Before installing **Jenkins**:

- Copy the project *gradle basic demo* from the class assignment 2 to a new folder `ca5`;
- Commit and push the changes to the repository:

  ```shell
  % git status
  % git add .
  % git commit -m "copy gradle basic demo"
  % git push origin master 
  ```  

To install **Jenkins** there are several ways of doing it, the recommended options are:

- Using Docker Container;

- Running directly the *War*.

In this case, the installation was done running directly the Web application ARchive (WAR) file version of Jenkins. This
file can be installed on any operating system or platform that runs a version of Java supported by Jenkins.

To install **Jenkins** using the *War* file the next steps must be followed:

- Download the **Jenkins** [WAR file](https://www.jenkins.io/download/);
- Copy the `WAR file` into the folder containing the *gradle basic demo* project, in this
  case `ca5/luisnogueira-gradle_basic_demo`;
- Open up a terminal window in the folder `ca5/luisnogueira-gradle_basic_demo`.
- Type in the terminal:

  ```shell
  % java -jar jenkins.war
  ```

![Java WAR](images/java-war1.png)

![Java WAR 2](images/java-war2.png)

- Open a new browser in <http://localhost:8080> and wait until the **Jenkins** setup page appears;
- Select install the suggested plugins;

![jenkinsStarting](images/localhost8080_starting.png)

- After the installation of the plugins, **Jenkins** will be ready to use.

![jenkinsStarting](images/localhost8080_ready.png)

## 2. Implementation

After installing and configuring **Jenkins**, we can now create a pipeline. Previously, it was necessary to set the
repository public to complete the class assignments. Therefore, in order to test the credentials, the repository must be
set to private again. In addition, we have to guarantee that the gradle wrapper file is in the repository.

### 2.1. Create a pipeline

### 2.1.1. Using a Private Repository

To create the **Jenkins** credentials, the following steps must be followed:

- Set the repository to private again;

- Now, in the main page of **Jenkins**, click on `Manage Jenkins` and after on `Manage Credentials`;

![Manage Credentials](images/manageJenkins.png)

- Click on **Jenkins** under the `Stores scoped to Jenkins` and after click on `Global credentials`;

![Repository Settings](images/credentialsJenkins.png)

![Repository Settings](images/globalcredentials.png)

- Click on `Add Credentials` to add a new set of credentials;

![Add Credentials](images/addCredentials.png)

- The set of credentials has:

Username : *Bitbucket* username;

Password : *Bitbucket* password;

ID: Internal ID which identifies these credentials.

![Global Credentials](images/globalCredentials_add.png)

![Final Credentials](images/finalCredentials.png)

### 2.2. Define the stages in the pipeline

After adding the necessary credentials, it is possible to create a Job to run a pipeline. First, define the stages in
the pipeline script and after the job can be created.

#### 2.2.1. Prepare the Jenkinsfile

The definition of a Jenkins Pipeline is usually written into a text file (named `Jenkinsfile`) in the **Groovy**. It supports two syntaxes, Declarative and Scripted Pipeline. This file is usually located at the root folder of
the source code repository.

- Configure the `Jenkinsfile`:

```Groovy
pipeline {
    agent any
    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: 'mlf-bitbucket-credentials', url:
                        'https://bitbucket.org/MariaLuisFok/devops-20-21-1201772'
            }
        }
        stage('Assemble') {
            steps {
                echo 'Assembling...'
                dir('ca5/luisnogueira-gradle_basic_demo') {
                    sh './gradlew assemble'
                }
            }
        }
        stage('Test') {
            steps {
                echo 'Testing...'
                dir('ca5/luisnogueira-gradle_basic_demo') {
                    sh './gradlew test'
                }
            }
            post {
                always {
                    junit '**/build/test-results/test/TEST-*.xml'
                }
            }
        }
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                archiveArtifacts 'ca5/luisnogueira-gradle_basic_demo/build/distributions/*'
            }
        }
    }
}
```

This `Jenkinsfile` has the following *stages*:

- **Agent** - The agent directive, which is required, instructs Jenkins to allocate an executor and workspace for the
  Pipeline.
- **Checkout** - This stage is going to checkout the source code for the project on the defined URL, using the
  credentials we have defined previously.
- **Assemble** - This stage is going to run the *Gradle* `assemble` task of the project, compiling and producing the
  artifacts.
- **Test** - This stage runs the `test` task of *Gradle* in the project, this will run all the unit tests and publish
  in **Jenkins** the test results.
- **Archiving** - This stage is going to archive the build artifacts, generated during the `assemble` task (e.g., jar
  files). These files can be downloaded in the main page of **Jenkins**.

### 2.2.2. Testing the Jenkinsfile

For testing purposes, the `Jenkinsfile` can be tested by creating a new job and choosing the *Pipeline* option instead
of the *Pipeline script from SCM*. After, we can test our pipeline, by running it. When confident with the pipeline, we
can send the `Jenkinsfile` to the repository:

- Commit and push the `Jenkinsfile`, located at `ca5/luisnogueira-gradle_basic_demo` to the repository:

```shell
% git add
% git commit -m "add Jenkinsfile"
% git push origin master
```

#### 2.2.3. Create a job

To have access to the test results report, check if the **JUnit** plugin is already installed in **Jenkins**.

To create a job, the next steps should be followed:

- Access <http://localhost:8080> and then click on `create new item`;
- Enter an item name for the job and select the `Pipeline` option;

![Pipeline Name](images/pipelineJobName.png)

- After creating the job, select `Pipeline` and select the *Pipeline script from SCM*;
- Copy your repository URL to the `Repository URL` field;
- Select the credentials previously created;

![Pipeline Jenkins Script](images/pipelineJenkins.png)

- Change the `Script Path` to the location where the `Jenkinsfile` is, in this case the `Jenkinsfile`is located
  at `ca5/luisnogueira-gradle_basic_demo/Jenkinsfile`;

![Script Path](images/scriptPathca5.png)

- Save the changes;

- We can now run the new CI/CD pipeline in **Jenkins**.

#### 2.2.2. Run the Build Now

The first time we run the build, the test report graph will not appear. Refresh the page, and the graph will then
appear.

![Build Success With One Test](images/buildSuccessOneTest.png)

- To experiment with the test reports, add a new test, for example a test for the ChatClient class and build again:

```java
@Test
public void testChatClientNotNull(){
        String serverAddress="serverAddress";
        int serverPort=8080;
        ChatClient chatClient=new ChatClient(serverAddress,serverPort);
        assertNotNull(chatClient);
        }
```

![Build Success With Two Tests](images/buildSuccessTwoTests.png)

- In addition, add a failing test, check that the build has failed and that the test report has also a new test which is
  failing:

```java

@Test
public void testChatClientNull(){
        String serverAddress="serverAddress";
        int serverPort=8080;
        ChatClient chatClient=new ChatClient(serverAddress,serverPort);
        assertNull(chatClient);
        }
```

![Build Fails With One Failing Test](images/BuildFailedWithOneFailingTest.png)

- Finally, correct the failing test and run the build again;

![Build Final Success](images/FinalBuildSuccess.png)

- Click on the build and check the `Console output`.

![Console Output Success](images/consoleOutput.png)
![Console Output Success 2](images/consoleOutput2.png)

Inside the build, we have access to several options, such as the `Console output`, the `Build Artifacts`, `Test Result`
and `Workspaces`.

In the `Console output` we can check all the stages and steps performed during the build, we can also understand, if the
build fails, what is the error.

The `Build Artifacts` has the generated files during the `assemble` stage and that we have instructed **Jenkins** to
archive.

The `Test Result` has all the tests and their status.

In addition, the `Workspaces` directory is where **Jenkins** builds the project, contains the source code that was
checked out and the generated files. The workspace is reused for each build.

![Build Options](images/BuildOptions.png)

## 3. Mark the repository with the tag ca5

Commit the changes made in the readme file:

```shell
% git commit -a -m "update readme file"
% git push origin master
```

To mark the end of part one of this assignment, use the tag ca5-part1 in the repository:

```shell
% git tag -a ca5-part1 -m "end of class assignment 5 part 1"
% git push origin ca5-part1
```

## 4. References

<https://www.jenkins.io/doc/book/installing/war-file/>

<https://www.jenkins.io/download/>

<https://plugins.jenkins.io/junit/#documentation>

<https://stackoverflow.com/questions/47928927/compile-groovy-project-and-run-junit-tests-via-jenkins>

<https://vocon-it.com/2017/01/06/getting-started-with-jenkins-part-4-1-functional-java-tests-via-junit/>
