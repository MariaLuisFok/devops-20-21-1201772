package basic_demo;

import junit.framework.TestCase;
import org.junit.Test;

public class ChatClientTest extends TestCase {


    @Test
    public void testChatClientNotNull() {
        String serverAddress = "serverAddress";
        int serverPort = 8080;
        ChatClient chatClient = new ChatClient(serverAddress, serverPort);
        assertNotNull(chatClient);
    }

    @Test
    public void testChatClientNotNullOtherServerPort() {
        String serverAddress = "serverAddress";
        int serverPort = 8081;
        ChatClient chatClient = new ChatClient(serverAddress, serverPort);
        assertNotNull(chatClient);
    }

}