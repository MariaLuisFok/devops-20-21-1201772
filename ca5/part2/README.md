# Class Assignment 5 Report - Part 2

## 1. Continuous Integration/ Continuous Development pipelines with Jenkins

The goal of this fifth class assignment part two, is to use **Jenkins** to build the *tutorial spring boot application*
developed in the class assignment 2 part 2, by creating a pipeline.

### 2. Implementation

Before creating the pipeline in **Jenkins**:

- Copy the project *tutorial spring boot application* from the class assignment 2 part 2 to a new folder `ca5/part2`;
- Commit and push to the repository:

  ```shell
  % git status
  % git add react-and-spring-data-rest-basic
  % git commit -m "ca5_part2 copy tut-gradle-basic"
  % git push origin master 
  ```  

- Open the browser in <http://localhost:8080> and enter the credentials previously defined in the part 1 of this
  assignment;

### 2.1. Create a pipeline

### 2.1.1 Define the stages in the pipeline

This `Jenkinsfile` will have the following *stages*:

1. **Agent** - The agent directive, which is required, instructs Jenkins to allocate an executor and workspace for the
   Pipeline.

2. **Checkout** - This stage is going to checkout the source code for the project on the defined URL, using the
   credentials we have defined previously.

3. **Assemble** - This stage is going to run the *Gradle* `assemble` task of the project, compiling and producing the
   artifacts.

4. **Test** - This stage runs the `test` task of *Gradle* in the project, this will run all the unit tests and publish
   in **Jenkins** the test results.

    - To have access to the test results report, check if the **JUnit** plugin is already installed in **Jenkins**.

    - To check if the plugin is installed, go to `Manage Jenkins`, `Manage Plugins` and search in the `Installed` tab.

   ![JUnit Plugin](images/junitPlugin.png)

5. **JavaDoc** - Generates the javadoc of the project and publish it in **Jenkins**.

    - To publish the HTML javadoc report of the project, it is necessary to install the **HTML Publisher** plugin.
    - To install the plugin, go to `Manage Jenkins`, `Manage Plugins` and search in the `Available` tab.

   ![HTML Plugin](images/pluginHTML.png)

6. **Archiving** - This stage is going to archive the build artifacts, generated during the `assemble` task (i.e., war
   files). These files can be downloaded in the main page of **Jenkins**;

7. **DockerImage** - This stage generates a **Docker** image with Tomcat and the war file and publishes it in the
   **Docker** Hub.
    - To generate the **Docker** image, first it is necessary to add the *web* `Dockerfile` to the repository in the
      same folder as the `Jenkinsfile` and update the *WORKDIR* in the `Dockerfile` to the current directory of the
      project `/tmp/build/devops-20-21-1201772/ca5/part2/react-and-spring-data-rest-basic`;
    - Install the **Docker Pipeline** plugin, to allow the building, testing, and using Docker images from **Jenkins**
      Pipeline projects.
    - When doing push to the `Docker Hub` through **Jenkins**, **Docker** must be running in the background.

#### 2.1.1.2 Prepare the Jenkinsfile

- Configure the `Jenkinsfile`, this file is usually located at the root folder of the source code repository:

```Groovy
pipeline {
    agent any
    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: 'mlf-bitbucket-credentials', url:
                        'https://bitbucket.org/MariaLuisFok/devops-20-21-1201772'
            }
        }
        stage('Assemble') {
            steps {
                echo 'Assembling...'
                dir('ca5/part2/react-and-spring-data-rest-basic') {
                    sh './gradlew assemble'
                }
            }
        }
        stage('Test') {
            steps {
                echo 'Testing...'
                dir('ca5/part2/react-and-spring-data-rest-basic') {
                    sh './gradlew test'
                }
            }
            post {
                always {
                    junit '**/build/test-results/test/TEST-*.xml'
                }
            }
        }
        stage('JavaDoc') {
            steps {
                echo 'Generating JavaDoc...'
                dir('ca5/part2/react-and-spring-data-rest-basic') {
                    sh './gradlew javadoc'

                    publishHTML(target: [
                            allowMissing         : false,
                            alwaysLinkToLastBuild: true,
                            keepAll              : true,
                            reportDir            : 'build/docs/javadoc',
                            reportFiles          : 'index.html',
                            reportName           : "Javadoc Report"
                    ])
                }
            }
        }
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                archiveArtifacts 'ca5/part2/react-and-spring-data-rest-basic/build/libs/*'
            }
        }
        stage('Docker Image') {
            steps {
                script {
                    docker.withRegistry('https://registry.hub.docker.com', 'mlf-docker-credentials') {
                        def customImage = docker.build("marialuis/devops-20-21-1201772", "ca5/part2/web")
                        customImage.push("${env.BUILD_NUMBER}")
                    }
                }
            }
        }
    }
}
```

In the JavaDoc Stage, the **HTML Publisher** plugin, has the following step parameters:

```Groovy
publishHTML(target: [
        allowMissing         : false,
        alwaysLinkToLastBuild: true,
        keepAll              : true,
        reportDir            : 'build/docs/javadoc',
        reportFiles          : 'index.html',
        reportName           : "Javadoc Report"
])
```

- allowMissing : If checked, will allow report to be missing and build will not fail on missing report;
- alwaysLinkToLastBuild : If this control and "Keep past HTML reports" are checked, publish the link on project level
  even if build failed;
- keepAll : If checked, archive reports for all successful builds, otherwise only the most recent;
- reportDir : The path to the HTML report directory relative to the workspace;
- reportFile : The file(s) to provide links inside the report directory;
- reportName : The name of the report to display for the build/project, such as "Code Coverage"

The JavaDoc report will be available in the main page of the Pipeline.

In the Docker Image Stage, the Docker Registry requires authentication, therefore add a "
Username/Password" Credentials item from the Jenkins home page and use the Credentials ID as a second argument to
withRegistry(), as previously done with the bitbucket credentials. The image is built from a `Dockerfile` in the same
folder as the `Jenkinsfile`. The tag for the image will be the job build number of Jenkins.

### 2.2.2. Testing the Jenkinsfile

For testing purposes, the `Jenkinsfile` can be tested by creating a new job and choosing the *Pipeline* option instead
of the *Pipeline script from SCM*. After, we can test our pipeline, by running it. When confident with the pipeline, we
can send the `Jenkinsfile` to the repository:

- Commit and push the `Jenkinsfile`, located at `ca5/part2/react-and-spring-data-rest-basic` to the repository:

```shell
% git add
% git commit -m "add Jenkinsfile"
% git push origin master
```

#### 2.1.2. Create a job

To create a job, the next steps should be followed:

- Access <http://localhost:8080> and then click on `create new item`;
- Enter an item name for the job and select the `Pipeline` option;
- After creating the job, select `Pipeline` and select the *Pipeline script from SCM*;
- Copy your repository URL to the `Repository URL` field;
- Select the credentials previously created;
- Change the `Script Path` to the location where the `Jenkinsfile` is, in this case the `Jenkinsfile`is located
  at `ca5/part2/react-and-spring-data-rest-basic/Jenkinsfile`;

![Script Path](images/pipelineGit.png)

- Save the changes;

- Open **Docker** in the background;

- We can now run the new CI/CD pipeline in **Jenkins**.

#### 2.2. Run the Build Now

- Click on `Build Now` and check the `Stage View`, after the build is finished;

![Build Success](images/1stBuildSuccess.png)
![Build Success Status](images/FirstBuildStatus.png)

- Add a failing test, check that the build has failed and that the test report has also a new test which is failing:

```java
@Test
    void ensureEmployeeCreatedSuccessfullyAndFirstNameEquals(){
            String firstName="Samwise";
            String lastName="Gamgee";
            String description="Frodo Baggins best friend";
            String job="Mayor of the Shire";
            String email="samwise.gamgee@gmail.com";

            Employee anEmployee=new Employee(firstName,lastName,description,job,email);

            assertNotNull(anEmployee);
            assertEquals(lastName,anEmployee.getFirstName());
            }
```

![Build Fails ](images/2ndBuildFailed.png)
![Build Fails Status](images/SecondBuildFail.png)

- Check the error in the Console Output:

![Build Fails With One Failing Test](images/ConsoleOutput_2ndBuildFail.png)

In the console output, it is possible to verify that the failing test is the
ensureEmployeeCreatedSuccessfullyAndFirstNameEquals().

- Finally, correct the failing test and run the build again;

![Build Final Success](images/Build Final Success.png)

Check if the JavaDoc report is available in the main page of the Pipeline:

![JavaDoc report](images/javaDocJenkins.png)

- Click on `JavaDoc Report`:

![JavaDoc report](images/JavaDocReport.png)

- Check in the **Docker Hub** repository, if the image was pushed:

![Docker Hub](images/1stPublishDockerHub.png)

## 3. Alternative

The alternative to be explored in this section of the class assignment is **Gitlab**.

### 3.1. Jenkins vs GitLab

**Jenkins** is one of the most popular self-managed open source build automation and CI developer tools. It derives its
incredible flexibility from incorporating capabilities from hundreds of available plugins, enabling it to support
building, deploying, and automating any project.

**GitLab** is self-hosted and provides end-to-end DevOps capabilities and for each stage of the software development
lifecycle. **GitLab’s** Continuous Integration capabilities enable development teams to automate building and testing
their code.

The main advantages of **GitLab** when compared with **Jenkins** are:

- **GitLab** is a complete DevOps platform, includes SCM, testing, issue tracking, monitoring and CI/CD, while **
  Jenkins** is only a Continuous Integration too;
- **GitLab** delivers a single application, while **Jenkins** relies on integrating plugins for basic capabilities (SCM
  integration, Tests Reports, **Docker** Integration and more);
- **GitLab** was built to be used in DevOps, containers and Kubernetes, and it is based on a modern technology (2014),
  as opposed to **Jenkins** which is an older technology (2004) and was not designed for supporting cloud-native
  development;
- **GitLab** has good security and privacy policies;
- With **GitLab**, all the CI configuration is part of the repository in the `.gitlab-ci.yml` file on a per-project
  basis. With **Jenkins** we have the project configurations as part of a Jenkinsfile and also in the configuration of
  the plugins;
- **GitLab** has a better User Experience when compared to **Jenkins**;
- **GitLab** has **Docker** Native support, Jobs run in **Docker** containers, on the other hand **Jenkins** needs
  plugins to run **Docker** and sometimes is not clear which is the best to use.
- Developers have full visibility of the entire development lifecycle, when working with **GitLab**.

The main disadvantages of **GitLab** when compared with **Jenkins** are:

- Artifacts need to be defined and uploaded/downloaded for each job;
- **Jenkins** is *Hosted-Internally* and is a *free open source*, on the other side, **GitLab** CI/CD is *Self-Hosted*
  and has to be paid for more premium options;
- **GitLab** has also a higher learning curve due to the amount of features included.

### 3.2. Setup

- Visit the **Gitlab** [webpage](https://gitlab.com) and click on the `Try GitLab for Free`.
- Select Gitlab via a web browser:
  ![Gitlab Web](images/gitlab_web.png)
- Import the project through the bitbucket URL and create a new project.

![Repo URL](images/repoByURL.png)

![Repo URL2](images/repoURLAndMirror.png)

- After finishing the import, our files from the repository will be available at the **GitLab** repository.

![Repo GitLab](images/repoGitLab.png)

### 3.3. Implementation

### 3.3.1. Edit the GitLab CI configuration

The pipeline in **Gitlab** is defined in the `.gitlab-ci.yml` file and as with `Jenkinsfile` has stages, which group
multiple independent jobs and run in a defined order, these jobs consist in a group of steps, and jobs run as part of a
larger pipeline. The `.gitlab-ci.yml` has to be in the root of the repository, which contains the CI/CD configuration.
**GitLab** will detect it, and an application called *GitLab Runner* runs the scripts defined in the `.gitlab-ci.yml`.

This `.gitlab-ci.yml` will have the following *stages*:

1. **Build** - This stage is going to run the *Gradle* `assemble` task of the project, compiling and producing the
   artifacts.

2. **Test** - This stage runs the `test` task of *Gradle* in the project, this will run all the unit tests and publish
   in **GitLab** the test results

3. **JavaDoc** - Generates the javadoc of the project. The files generated can be downloaded after the pipeline is
   finished;

4. **Archive** - This stage is going to archive the build artifacts, generated during the `assemble` task (i.e., war
   files). These files can be downloaded after the pipeline is finished.

5. **Docker-Build** - This stage generates a **Docker** image with Tomcat and the war file and publishes it in the **
   Docker** Hub.
    - To generate the **Docker** image, first it is necessary to add the *web* `Dockerfile` to the repository in the
      same folder as the `Jenkinsfile` and update the *WORKDIR* in the `Dockerfile` to the current directory of the
      project `/tmp/build/devops-20-21-1201772/ca5/part2/react-and-spring-data-rest-basic`;
    - In addition, it is necessary to create the variables to store our **Docker** Hub credentials;
    - Click on `Settings`, then on `CI/CD` and click `Expand` in the `Variables` section and finally, click
      on `Add variable`;

   ![Variables](images/variablesCICD.png)

   ![Add Variable](images/addVariable.png)

    - The variables have the following information:

        - CI_REGISTRY_USER : docker-hub-username;
        - CI_REGISTRY_PASSWORD : docker-hub-password
        - CI_REGISTRY : docker-hub-registry
        - CI_REGISTRY_IMAGE : docker-hub-repository

      The **username** and **password** have to be **masked**.

      To confirm the **Docker** hub registry type in the terminal:

        ```shell
      % docker info | grep Registry
      ```

6. **Pages** - This stage is going to publish the HTML javadoc report and publish it to the **GitLab Pages** server,
   **GitLab** always deploys the website from a very specific folder called public in the repository. In addition, the
   published webpage will be available at the **GitLab** default domain for **GitLab Pages** websites, `*.gitlab.io`.

#### 2.1.1.2 Prepare the .gitlab-ci.yml

- Configure the `.gitlab-ci.yml`, this file is located at the root folder of the source code repository. To edit
  the `.gitlab-ci.yml`, go to `CI/CD` and click on `Editor`.

![Editor Pipeline](images/pipelineEditor.png)

```yml
image: java:8-jdk

before_script:
  - cd ca5/part2/react-and-spring-data-rest-basic
  - export GRADLE_USER_HOME=`pwd`/.gradle
  - chmod +x ./gradlew


stages: # List of stages for jobs, and their order of execution

  - build
  - test
  - javadoc
  - archive
  - docker-build
  - pages

build-job: # This job runs in the build stage, which runs first, without the tests.
  stage: build
  script:
    - echo "Compiling the code..."
    - ./gradlew assemble
  artifacts:
    paths:
      - ca5/part2/react-and-spring-data-rest-basic/build/libs/*
      - echo "Compile complete."

unit-test-job: # This job runs in the test stage. # It only starts when the job in the build stage completes successfully.
  stage: test
  script:
    - echo "Running tests... "
    - ./gradlew test
  artifacts:
    when: always
    reports:
      junit: ca5/part2/react-and-spring-data-rest-basic/build/test-results/test/**/TEST-*.xml

javadoc-job:
  stage: javadoc
  script:
    - echo "Generating JavaDoc..."
    - ./gradlew javadoc
  artifacts:
    paths:
      - ca5/part2/react-and-spring-data-rest-basic/build/docs/javadoc/index.html

archive:
  stage: archive
  script:
    - echo "Archiving..."
  artifacts:
    paths:
      - ca5/part2/react-and-spring-data-rest-basic/build/libs/*

pages: # This job runs in parallel with the javadoc stage.
  stage: javadoc
  script:
    - ./gradlew javadoc
    - cp build/docs/javadoc/index.html public
    - ls public
  artifacts:
    paths:
      - public

docker-build:
  image: docker:latest
  stage: docker-build
  services:
    - docker:dind
  before_script:
    - cd ca5/part2/web
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY

  script:
    - docker build -t $CI_REGISTRY_IMAGE:webgitlab .
    - docker push $CI_REGISTRY_IMAGE:webgitlab
```

The *image* key grabs an image from the **Docker Hub**, and uses it as a base image. **GitLab** will base all the tests
based off this image. In this case, since our project is based on Java, the specified image is `java:8-jdk`.

The `before_script` tells **GitLab** to run what is specified here before anything else.

**GitLab** will be running each stage, step by step. If one of them fails, it prevents the following ones to run.

In the *Pages* Stage, will run the javadoc task and deploy the HTML page to a webpage.

In the *docker-build* Stage, we will use the “Docker-in-Docker” (*dind*) in which the registered runner uses the
**Docker** executor, and the executor uses a container image of **Docker**, provided by **Docker**, to run the CI/CD
jobs. Therefore, the **Docker** image has all the docker tools installed and can run the job script in context of the
image. The *image* key is going to download the latest available version of **Docker**, and uses it as a base image.
The `before_script` in the the *docker-build* Stage, tells **GitLab** to change the directory and to login into **Docker
Hub**, using the previously defined credentials, after this we will build and push the generated image to **Docker Hub**
.

### 2.2.2. Building the Project

When a change is made in the `.gitlab-ci.yml`, a commit is done, and the pipeline will run.

To test the pipeline, do the stages one by one and increment as you go.

- In the `CI/CD` section in the `Pipelines` tab, check the pipelines to see the result of the finished pipelines.

- E.g. of failed pipeline:

![Build Fail Gitlab](images/FailedPipeline.png)

- By clicking on the Job (docker-build) that has failed it is possible to check and examine the error:

![Build Fails With One Failing Test](images/PipelineFailsDueToDockerCredentials.png)

We can verify that this Job failed and as a consequence the pipeline, due to an error in the **Docker** Hub credentials.

- After correcting the error in the **Docker** Hub credentials and committing the changes, the pipeline run again and
  this time it was successful.

![Success Pipeline GitLab Until Docker ](images/pipelineSuccessDocker.png)
![Build Success Status Docker](images/pipelineSuccessDockerLogs.png)

This pipeline does not have the last Pages stage.

- Finally, by adding the last Pages stage, to deploy the HTML javadoc report, the pipeline was successful:

![Build Final Success](images/FinalGitLabPipelineSuccess.png)

- Check the available for download artifacts (test-report, jar file, javadocs), the artifacts are in the `.zip` format:

![Artifacts Download](images/artifacts_download.png)

- Check in the *Docker* Hub, if the image was pushed:

![Docker Hub](images/pushGitLabDockerHub.png)

- Check the tests, their duration, success rate, and the number of Passed tests:

![TestsGitLab](images/statisticsTest.png)

- Check if the JavaDoc report is available at the **GitLab** default domain for **GitLab Pages** websites, `*.gitlab.io`
  . To have access to our Pages, go to the `Settings` section and click on `Pages` and the link will be available.

![JavaDoc report](images/pagesLink.png)

- Even though the Job Pages finished successfully, and the deployment was done, it was not possible to open the
  published page of the javadoc HTML report:

![JavaDoc report Error](images/errorPageOfJavaDoc.png)

- At the end, we can check CI/CD Analytics and confirm the total number of run pipelines, how many failed, how many were
  successful and the success ratio.

![TestsGitLab](images/statistics_Pipeline.png)

## 4. Mark the repository with the tag ca5-part2

Commit the changes made in the readme file:

```shell
% git commit -a -m "update readme file"
% git push origin master
```

To mark the end of part one of this assignment, use the tag ca5-part2 in the repository:

```shell
% git tag -a ca5-part2 -m "end of class assignment 5 part 2"
% git push origin ca5-part2
```

## 4. References

<https://www.jenkins.io/download/>

<https://plugins.jenkins.io/junit/#documentation>

<https://plugins.jenkins.io/htmlpublisher/>

<https://www.jenkins.io/blog/2016/07/01/html-publisher-plugin/>

<https://www.jenkins.io/doc/pipeline/steps/htmlpublisher/>

<https://bmuschko.com/blog/jenkins-build-pipeline/>

<https://www.jenkins.io/doc/book/pipeline/docker/>

<https://medium.com/devopscurry/jenkins-is-getting-old-so-what-are-the-alternatives-in-2021-fd6ce6707465>

<https://docs.gitlab.com/ee/ci/unit_test_reports.html>

<https://gist.github.com/daicham/5ac8461b8b49385244aa0977638c3420>

<https://about.gitlab.com/blog/2016/07/29/the-basics-of-gitlab-ci/>

<https://docs.gitlab.com/ee/user/project/pages/index.html>

<https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html>

<https://docs.gitlab.com/ee/ci/variables/README.html#custom-environment-variables>

<https://docs.gitlab.com/ee/ci/yaml/README.html>

<https://docs.gitlab.com/ee/ci/README.html>

<https://docs.gitlab.com/ee/ci/variables/predefined_variables.html>

<https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html>

<https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_from_scratch.html>

<https://docs.gitlab.com/ee/user/project/pages/index.html>

<https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#authenticate-with-registry-in-docker-in-docker>

<https://hackernoon.com/configuring-gitlab-ci-yml-150a98e9765d>