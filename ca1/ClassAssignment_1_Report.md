# Class Assignment 1 Report

## 1. Analysis, Design and Implementation

### 1.1 Analysis

A version control system (VCS) helps to keep track of the modifications done to the code by recording the changes made
to the files. Therefore, a VCS helps the developing team to manage the changes being made to the source code. When using
branches each developer can work in the code and if the changes made are valid and functioning, they are merged to the
original source. [1]

The types of Version Control Systems are the following:

* Local Version Control Systems

* Centralized Version Control Systems

* Distributed Version Control Systems

Local Version Control Systems: Has a database that keeps all the changes to files under revision control, being one of
the simplest VCS. [1]

Centralized Version Control Systems : In the centralized version control systems, the server is just one master
repository and each user has their own working copy. After making the changes in the code, the user needs to commit to
the master repository and merge their own code into the master repository. [1] [2] [2]

Distributed Version Control Systems (DVCS): In the distributed version control systems instead of having just one
repository, each user has own working copy (local repository). After making the changes in the code, the *commit* will
reflect these changes in the local repository. To move the changes to the master repository, first a *merge* should be
done, and a *push* must be executed to send the changes to the master repository. To obtain the changes made by other
users a *pull* should be executed, in order to have these changes in the local repository. [1] [2] [2] [3] [3] [4] [4]

#### 1.1.1 Git

Git takes a picture of what all files look like at that moment and stores a reference to that snapshot, when a commit is
done, or the state of the project is changed. The data in Git is more like a stream of snapshots. The information is not
stored as a list of file-based changes. [5]

Git has three main states that each file can exist in : modified, staged, and committed [5]:

* Modified – The file has been changed but not committed.

* Staged - The modified file will go into the next commit snapshot.

* Committed – The file is safely stored in the local database.

### 1.3 Implementation

#### 1.3.1 Tag the initial version as *v1.2.0*

- To add a tag and send it to the master branch, in order to mark the initial version of the application as v1.2.0, type
  the following in the terminal:

```
  $ git tag -a v1.2.0 -m "initial version"
  $ git push origin v1.2.0
```

- If necessary, to remove a remote tag, type:

```
  $ git push --delete origin v1.2.0
```

- If necessary, to remove a remote tag, type:

```
  $ git tag -d v1.2.0
```

- To see all the tags in the current branch type the following:

```
  $ git tag -l 
  
    v1.2.0
    v1.3.0
    v1.3.1
    v1.3.2
    v1.3.3

```

#### 1.3.2 Create a new branch named *email-field*

- To add a new email field to the application, we will create a new branch where we will develop the new feature. To
  create a new branch:

```
  $ git branch email-field
```

- To check in which branch we are on, type:

```
  $ git checkout
```

*Commit and Push*

- After adding the support for the email field we need to commit the changes made to the new branch, to commit the
  changes see the following examples:

The command ***git status*** displays paths that have differences between the index file and the current HEAD commit,
paths that have differences between the working tree and the index file, and paths in the working tree that are not
tracked by Git (and are not ignored by gitignore). [6]

The command ***git add***, adds file contents to the index. [7]

```
    $ git status
    $ git add .
    $ git commit -m "add email field in Employee"
```

The command ***git commit -a -m*** is used to add the files and commit at the same time.

```
    $ git status
    $ git commit -a -m "add email field in DatabaseLoader "
```

- After doing the commits for all the changes, it is necessary to send it to the repository. To push the new branch (
  email-field), type in the terminal:

```
    $ git push origin email-field
```

#### 1.3.3 Merge email-field branch with master and tag it as *v1.3.0*

*Merge*

- The next step is to merge the changes into the master branch:
    1. First we will need to change to the master branch, after all files are *committed*. To change to the master:
        ```
            $ git checkout master
        ```
    2. Merge the email-field branch into the master branch:
        ```
            $ git merge email-field
        ```

    3. After the merge, we can commit the merge and push it to the master branch:
        ```
            $ git commit -a -m "merge email-field into default"  
            $ git push origin master
        ```

*Tags*

- To create a new tag with the updated version, type:

```
  $ git tag -a v1.3.0 -m "version with email-field"
  $ git push origin v1.3.0
```

#### 1.3.4 Create a new branch for bug fixing called *fix-invalid-email*

*Branch*

- To create a bug for fixing a bug in the email field to the application, we will create a new branch:

```
  $ git branch fix-invalid-email
```

*Commit and Push*

- After adding the validation of the email we will need to commit each change made to the new branch, to commit a change
  see the examples below:

```
    $ git status
    $ git add .
    $ git commit -m "add email format validation"
```

```
    $ git status
    $ git commit -a -m "add tests for invalid email"
```

- After doing the commits for the validation of the email and unit tests, it is necessary to send it to the repository,
  as previously done. Type in the terminal:

```
    $ git push origin fix-invalid-email
```

#### 1.3.5 Merge the branch to master with tag change, in the minor number as *v1.3.1*

*Merge*

- The next step is to merge the changes into the master branch:
    1. First we will need to change to the master branch:
        ```
            $ git checkout master
        ```
    2. Check the status of the files:
         ```
            $ git status
         ```
    2. Merge the fix-invalid-email branch into the master branch:
        ```
            $ git merge fix-invalid-email
        ```
    3. After the merge, we can commit the merge and push it to the master:
        ```
            $ git commit -m "merge fix-invalid-email into default"  
            $ git push origin master
        ```

*Tags*

- To create a new tag with the updated version and send it to the repository, type:

```
$ git tag -a v1.3.1 -m "version with invalid-email fixed"
$ git push origin v1.3.1
```

#### 1.3.6  Mark the repository with tag *ca1*

```
$ git tag -a ca1 -m "end ca1"
$ git push origin ca1
```

## 2. Analysis of an Alternative

The most popular alternatives to Git are Subversion, CVS, Mercurial Microsoft Team Foundation Server, Helix Core,
Plastic SCM and others. [8]

### 2.1 Mercurial

Mercurial is a free, distributed source control management tool. With Mercurial each developer has a local copy of the
entire development history, therefore it is not dependent of a network access, or a central server to commit the
changes, only to push the modifications. Committing, branching and merging are faster than with a centralized version
control system. Mercurial won't let developers to edit previous commits. Mercurial is well documented and easy for new
learners. [9] [10] [10] [11] [11]

## 2.1 Differences between Git and Mercurial

The main advantages and disadvantages of Git and Mercurial are:

**Git**

- Advantages

    - Capable of efficiently handling small to large sized projects;
    - Git's branch structure helps you avoid merging code in the wrong place;
    - Possibility to rewrite history, rebase/modify commits.

- Disadvantages
    - Usability, not user-friendly;
    - Large commands;
    - Documentation harder to understand.

**Mercurial**

- Advantages

    - Easy to learn;
    - Documentation easy to understand;
    - User-friendly for new users.

- Disadvantages
    - Rollback command will only undo the last commit, additional extensions must be used for more;
    - Cannot rewrite history.

To summarize, Git is known to have a higher learning curve and Mercurial is known to be easier to learn. [12]
When using Git simple actions can require complex actions to undo or refine. [11]
Git documentation’s is also harder to understand, while the Mercurial documentation is simpler. [13]

The biggest advantage of Git is that it has become an industry standard, which most developers and companies are using.
Mercurial biggest advantage is that it’s easier to learn and use, which is useful for less-technical content users. [13]

## 3. Implementation of the Alternative

The repository used to host the project
was: [HelixTeamHub](https://helixteamhub.cloud/switch/projects/devops-20-21-1201772/team)

### 3.1 Install and Setup

- The download for the installation of Mercurial is available in: https://www.mercurial-scm.org/downloads

- To install Mercurial, see: https://www.mercurial-scm.org/wiki/TutorialInstall


- After the installation, the command *hg* can be used to display the basic commands

```
$ hg
```

- In addition, the command *hg version* will prompt the version of Merecurial we are running

```
$ hg version
```

- To configure the email address and username used in the commits, a file .hgrc must be created in the home directory.
  To create the file and email address and username configuration, please follow the example below and add to the file
  the username and email as in the example below:

```
$ nano .hgrc
```

Example of username and email:

```
[ui]
username = Maria  Luís <1201772@isep.ipp.pt>
```  

### 3.2 Create a new repository

To create a new Mercurial repository there are two options:

1. Using a local directory and turn it into a Mercurial repository;

    - Enter the directory which you wish to import
      ```
       $ cd path/to/local/folder
      ```
    - Initialise the folder as a Mercurial repository
      ```
      $ hg init
      ```
    - Add all files and commit for the first time
      ```
      $ hg add
      $ hg commit -m "Initial Commit"
      ```
    - Send commits
      ```
      $ hg push https://1201772isepipppt@helixteamhub.cloud/switch/projects/devops-20-21-1201772/repositories/mercurial/devops-20-21-1201772
      ```

2. Clone an existing Mercurial repository.

    - Enter the directory in which you wish to clone the repository
      ```
         $ cd path/to/local/folder
      ```
    - Clone the repository
      ```
         $ hg clone https://1201772isepipppt@helixteamhub.cloud/switch/projects/devops-20-21-1201772/repositories/mercurial/devops-20-21-1201772
      ```

### 3.3 Goals/Requirements - Alternative Solution

The main branch in Mercurial is called default. This branch will be used to "publish" the "stable" version. After the
installation and setup of the mercurial and mercurial repository, we will work on the Tutorial React.js and Spring Data
REST application that was previously added to the repository.

To add the Tutorial React.js and Spring Data REST application, follow the next steps:

- Copy and Paste the folder tut-basic to the local repository directory;

- Add

```
  $ hg add
```

- Commit

```
  $ hg commit -m "nitial commit"
```

- Push

```
  $ hg push
```

#### 3.3.1 Implementation of the assignment

##### 3.3.1.1 Tag the initial version as *v1.2.0*

- To add a tag and send it to the default branch, in order to mark the initial version of the application as v1.2.0,
  type the following in the terminal:

```
  $ hg tag v1.2.0 -m "initial version"
  $ hg push
```

- To remove a tag, if necessary type:

```
  $ hg tag --remove v1.2.0 
  $ hg push
```

- To see all the tags in the current branch type the following:

```
  $ hg tags 
  
    tip                               40:494c8f480892
    v1.3.3                            39:085c33d5d1a5
    v1.3.2                            33:d662f5cac189
    v1.3.1                            28:84a02b5e424c
    v1.3.0                            23:9654d7821ab6
    v1.2.0                             4:97e883ab9128

```

##### 3.3.1.2 Create a new branch named *email-field*

- To add a new email field to the application, we will create a new branch where we will develop the new feature. To
  achieve this we are creating a named branch:

```
  $ hg branch email-field
```

- To check in which branch we are on, type:

```
  $ hg branch
```

- After adding the support for the email field we need to commit the changes made to the new branch, to commit the
  changes see the following example:

```
    $ hg status
    $ hg commit -m "add EmployeeTest"
    $ hg sum  
```

![img.png](./img.png)

The command ***hg status*** lists all the changed files in the working directory:

The codes used to show the status of files are:

M = modified; A = added; R = removed; C = clean;
! = missing (deleted by non-hg command, but still tracked); ? = not tracked; I = ignored; = origin of the previous
file (with --copies).

The command ***hg sum*** shows a brief summary of the working directory state, including parents, branch, commit status,
phase and available updates.

- After doing the commits for all the changes, it is necessary to send it to the repository. Since we are in a new
  branch, Mercurial won't let you accidentally push a new branch. To push the new branch (email-field), type in the
  terminal:

```
    $ hg push -b . –new-branch
```

![img_1.png](./img_1.png)

##### 3.3.1.3 Merge email-field branch with master and tag it as *v1.3.0*

- The next step is to merge the changes into the default branch:
    1. First we will need to change to the default branch, we have two options, either use the hg up command, or the hg
       checkout command:
        ```
            $ hg up default
        ```
       or
        ```
            $ hg checkout default
        ```
    2. Merge the email-field branch into the default branch:
        ```
            $ hg merge email-field
        ```

    3. After the merge, we can commit the merge and push it to the default branch:
        ```
            $ hg commit -m "merge email-field into default"  
            $ hg push
        ```
- To create a new tag with the updated version, type:

```
  $ hg tag v1.3.0 -m "version with email-field"
  $ hg push
```

##### 3.3.1.4 Create a new branch for bug fixing called *fix-invalid-email*

- To fix a bug in the email field to the application, we will create a new branch:

```
  $ hg branch fix-invalid-email
```

- After adding the validation of the email we will need to commit each change made to the new branch, to commit a change
  see the example below:

```
    $ hg status
    $ hg commit -m "fix bug in email in DatabaseLoader"
```

- After doing the commits for all the changes, it is necessary to send it to the repository, as previosuly done. Type in
  the terminal:

```
    $ hg push -b . –new-branch
```

##### 3.3.1.5 Merge the branch to master with tag change, in the minor number as *v1.3.1*

- The next step is to merge the changes into the default branch:
    1. First we will need to change to the default branch:
        ```
            $ hg up default
        ```
       ![img_2.png](./img_2.png)

    2. Check the status of the files:
         ```
            $ hg status
         ```

    3. Merge the fix-invalid-email branch into the default branch:
        ```
            $ hg merge fix-invalid-email
        ```
       ![img_3.png](./img_3.png)
    4. After the merge, we can commit the merge and push it to the default branch:
        ```
            $ hg commit -m "merge fix-invalid-email into default"  
            $ hg push
        ```
- To create a new tag with the updated version and send it to the repository, type:

```
  $ hg tag v1.3.1 -m "version with invalid-email fixed"
  $ hg push
```

![img_4.png](./img_4.png)

##### 3.3.1.6 Mark the repository with tag *ca1*

```
  $ hg tag ca1 -m "end ca1"
  $ hg push
```

## 4. References

[1] https://www.geeksforgeeks.org/version-control-systems/

[1]: https://www.geeksforgeeks.org/version-control-systems/

[2] https://www.atlassian.com/blog/software-teams/version-control-centralized-dvcs

[2]: https://www.atlassian.com/blog/software-teams/version-control-centralized-dvcs

[3] https://www.geeksforgeeks.org/centralized-vs-distributed-version-control-which-one-should-we-choose/?ref=rp

[3]: https://www.geeksforgeeks.org/centralized-vs-distributed-version-control-which-one-should-we-choose/?ref=rp

[4] https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control

[4]: https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control

[5] https://git-scm.com/book/en/v2/Getting-Started-What-is-Git%3F

[5]: https://git-scm.com/book/en/v2/Getting-Started-What-is-Git%3F

[6] https://git-scm.com/docs/git-status

[6]: https://git-scm.com/docs/git-status

[7] https://www.g2.com/products/git/competitors/alternatives

[7]: https://www.g2.com/products/git/competitors/alternatives

[8] https://git-scm.com/docs/git-add

[8]: https://git-scm.com/docs/git-add

[9] http://hgbook.red-bean.com/read/preface.html

[9]: http://hgbook.red-bean.com/read/preface.html

[10] https://www.mercurial-scm.org

[10]: https://www.mercurial-scm.org

[11] https://content.intland.com/blog/sdlc/why-is-git-better-than-mercurial

[11]: https://content.intland.com/blog/sdlc/why-is-git-better-than-mercurial

[12] http://hgbook.red-bean.com/read/how-did-we-get-here.html

[12]: http://hgbook.red-bean.com/read/how-did-we-get-here.html

[13] https://www.perforce.com/blog/vcs/git-vs-mercurial-how-are-they-different

[13]: https://www.perforce.com/blog/vcs/git-vs-mercurial-how-are-they-different

[14] https://www.softwaretestinghelp.com/version-control-software/

[14]: https://www.softwaretestinghelp.com/version-control-software/

[15]:  https://www.mercurial-scm.org/wiki/MercurialHosting
[15] https://www.mercurial-scm.org/wiki/MercurialHosting

[16]:https://www.mercurial-scm.org/wiki/Branch
[16] https://www.mercurial-scm.org/wiki/Branch

[17]: https://www.mercurial-scm.org/wiki/Tag
[17] https://www.mercurial-scm.org/wiki/Tag

[18]: https://stevelosh.com/blog/2009/08/a-guide-to-branching-in-mercurial/
[18] https://stevelosh.com/blog/2009/08/a-guide-to-branching-in-mercurial/

[19]: https://helixteamhub.cloud/switch/dashboard
[19] https://helixteamhub.cloud/switch/dashboard
